import traceback
from logging import ERROR, CRITICAL, INFO, DEBUG

from flask import current_app, make_response
from flask_babel import gettext


class LoglevelException(Exception):
    code = 500

    def __init__(self, message: str = None, loglevel: int = ERROR):
        super().__init__(message)
        # https://flask.palletsprojects.com/en/1.1.x/logging/
        # https://docs.python.org/3/library/logging.html#levels
        if not isinstance(loglevel, int):
            self.loglevel = ERROR
        else:
            self.loglevel = loglevel

    def log(self, msg: str = None):
        if msg is None:
            msg = str(self)

        try:
            current_app.logger.log(self.loglevel, msg)
        except TypeError:
            current_app.logger.error(gettext("Unknown logging level for error: {err}").format(err=self.loglevel))
            current_app.logger.error(msg)

    def response(self, log=False):
        msg = str(self)
        if log:
            self.log(msg)
        return make_response(msg, self.code)


class NotAuthenticated(LoglevelException):
    code = 401
    loglevel = INFO


class NotAuthorized(LoglevelException):
    code = 403
    loglevel = INFO


class BadRequest(LoglevelException):
    code = 400
    loglevel = INFO


class InvalidUrl(LoglevelException):
    code = 404
    loglevel = DEBUG


class RequestNotFound(LoglevelException):
    code = 404
    loglevel = DEBUG


class NotAcceptable(LoglevelException):
    code = 406
    loglevel = INFO


class MethodNotAllowed(LoglevelException):
    code = 405
    loglevel = DEBUG


class InternalError(LoglevelException):
    code = 500

    def __init__(self, message: str = None, loglevel: int = CRITICAL):
        super().__init__(message, loglevel)


def log_failure_and_continue(function):
    def fcn(*args, **kwargs):
        try:
            result = function(*args, **kwargs)
            if not result:
                current_app.logger.error("{} failed.".format(function.__name__))
        except Exception as exc:
            current_app.logger.error("{0} failed: {1}\n{2}".format(function.__name__, str(exc), traceback.format_exc()))
            result = False

        return result

    return fcn


class RedirectException(Exception):
    def __init__(self, response):
        self.response = response
