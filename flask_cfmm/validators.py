from ipaddress import ip_network, IPv4Network, IPv6Network

from wtforms import Form, Field
from wtforms.validators import ValidationError


class IPNetwork(object):
    """
    Validates an IP network.

    :param ipv4:
        If True, accept IPv4 networks as valid (default True)
    :param ipv6:
        If True, accept IPv6 networks as valid (default False)
    :param message:
        Error message to raise in case of a validation error.
    """

    def __init__(self, ipv4: bool = True, ipv6: bool = False, message: str = None) -> None:
        if not ipv4 and not ipv6:
            raise ValueError("IP Address Validator must have at least one of ipv4 or ipv6 enabled.")
        self.ipv4 = ipv4
        self.ipv6 = ipv6
        self.message = message

    def __call__(self, form: Form, field: Field) -> None:
        value = field.data
        valid = False
        if value:
            valid = (self.ipv4 and self.check_ipv4(value)) or (self.ipv6 and self.check_ipv6(value))

        if not valid:
            message = self.message
            if message is None:
                message = field.gettext("Invalid IP network.")
            raise ValidationError(message)

    @classmethod
    def check_ipv4(cls, value: str) -> bool:
        try:
            address = ip_network(value)
        except ValueError:
            return False

        if not isinstance(address, IPv4Network):
            return False

        return True

    @classmethod
    def check_ipv6(cls, value: str) -> bool:
        try:
            address = ip_network(value)
        except ValueError:
            return False

        if not isinstance(address, IPv6Network):
            return False

        return True
