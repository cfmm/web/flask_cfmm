import base64
import hashlib
import hmac
import json
import traceback
import typing as t
from ipaddress import ip_address, ip_network
from logging import DEBUG
from uuid import uuid1, UUID

import flask_security
from alembic_utils.pg_function import PGFunction
from alembic_utils.pg_trigger import PGTrigger
from flask import current_app, make_response, request
from flask_babel import gettext, lazy_gettext
from flask_login import current_user
from sqlalchemy import (
    Boolean,
    DateTime,
    TIMESTAMP,
    Column,
    Integer,
    String,
    ForeignKey,
    inspect,
    Table,
    func,
    text,
    FetchedValue,
    false,
)
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship, validates, declarative_mixin
from sqlalchemy.sql.elements import ClauseElement

from .configure import get_role
from .exceptions import (
    NotAuthenticated,
    NotAuthorized,
    BadRequest,
    InvalidUrl,
    RequestNotFound,
    NotAcceptable,
    MethodNotAllowed,
    LoglevelException,
)
from .migration.sqllite_trigger import SQLiteTrigger
from .types import IPNetworkType

if t.TYPE_CHECKING:
    from flask_security.datastore import Role


class ModifiedDefault(ClauseElement):
    name = "modified_default"

    def _compiler_dispatch(self, visitor: t.Any, **kw: t.Any) -> str:
        if visitor.dialect.name in ("mysql",):
            # MySQL can use the special "ON UPDATE CURRENT_TIMESTAMP"
            source = text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
        else:
            source = func.current_timestamp()

        return source.compile().string


@declarative_mixin
class DateTracking(object):
    created = Column(
        "created",
        TIMESTAMP(),
        nullable=False,
        server_default=func.now(),
    )
    modified = Column(
        "modified",
        TIMESTAMP(),
        nullable=False,
        server_default=ModifiedDefault(),
        server_onupdate=FetchedValue(),
    )

    @classmethod
    def build_entities(cls, table, connection, **kwargs):
        # Create the triggers on the tables
        # Triggers are automatically dropped with the table in sqlite and postgresql, so only creation requires
        # special handling.

        # noinspection PyUnresolvedReferences
        entities = super().build_entities(table, connection, **kwargs)

        dialect = connection.dialect.name
        column = table.c.modified

        preparer = connection.dialect.identifier_preparer
        table_name = preparer.format_table(table)
        column_name = preparer.format_column(column)

        # Add SQLite support
        if dialect == "sqlite":
            # Compute the primary key matching criteria
            keys = list()
            for c in table.primary_key.columns:
                keys.append(f"{preparer.format_column(c)}=OLD.{preparer.format_column(c)}")
            pk = " AND ".join(keys)
            if not pk:
                raise ValueError(gettext("Table {table} does not have primary key").format(table=table.name))

            schema = preparer.quote_schema(table.metadata.schema or "main")

            trigger = SQLiteTrigger(
                schema=schema,
                signature=f"{table.name}_update_{column.name}",
                on_entity=f"{schema}.{table_name}",
                definition=f"AFTER UPDATE ON {schema}.{table_name} "
                "FOR EACH ROW "
                "BEGIN "
                f"UPDATE {table_name} SET {column_name}=CURRENT_TIMESTAMP WHERE {pk};"
                "END;",
            )
            entities.append(trigger)

        elif dialect == "postgresql":
            # Add support for postgresql
            schema = preparer.quote_schema(table.metadata.schema or "public")
            signature = f"update_{table.name}_{column_name}_column()"

            update_function = PGFunction(
                schema=schema,
                signature=signature,
                definition="RETURNS TRIGGER AS $$ "
                "BEGIN "
                f"NEW.{column_name} = CURRENT_TIMESTAMP;"
                "RETURN NEW;"
                "END;"
                "$$ language 'plpgsql';",
            )

            trigger = PGTrigger(
                schema=schema,
                signature=f"{table.name}_update_{column.name}",
                on_entity=f"{schema}.{table_name}",
                definition=f"BEFORE UPDATE ON {schema}.{table_name} FOR EACH ROW "
                f"EXECUTE PROCEDURE {schema}.{signature};",
            )

            entities.append(update_function)
            entities.append(trigger)

        return entities


@declarative_mixin
class RoleMixin(flask_security.RoleMixin):
    __tablename__ = "role"
    id = Column(Integer(), primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255))

    def __str__(self):
        return self.name


class RoleManager(object):
    def __init__(self, user):
        self.user = user

    def __iter__(self):
        for item in self.user.local_roles:
            yield item

        token = getattr(self.user, "token", None)
        if token:
            for item in token.roles:
                yield item

    def append(self, item):
        if isinstance(item, self.user.role_model):
            self.user.local_roles.append(item)


@declarative_mixin
class UserMixin(flask_security.UserMixin):
    __tablename__ = "user"
    role_model = None

    id = Column(Integer, primary_key=True)
    fs_uniquifier = Column(String(255), unique=True, nullable=False)
    username = Column(String(255), unique=True, nullable=False)
    last_login_at = Column(DateTime(timezone=True))
    current_login_at = Column(DateTime(timezone=True))
    last_login_ip = Column(String(100))
    current_login_ip = Column(String(100))
    login_count = Column(Integer)
    active = Column(Boolean())

    @declared_attr
    def local_roles(cls):
        # noinspection PyUnresolvedReferences
        roles_users = Table(
            "roles_users",
            cls.metadata,
            Column("id", Integer(), primary_key=True),
            # Note that SQLAlchemy fails to set the name of user_id using naming conventions and it must be set
            # explicitly
            Column(
                "user_id",
                cls.id.expression.type,
                ForeignKey(
                    cls.id, name=f"fk_roles_users_user_id_{cls.__tablename__}", ondelete="CASCADE", onupdate="CASCADE"
                ),
                index=True,
                nullable=False,
            ),
            Column(
                "role_id",
                cls.role_model.id.expression.type,
                ForeignKey(cls.role_model.id, ondelete="CASCADE", onupdate="CASCADE"),
                index=True,
                nullable=False,
            ),
        )
        setattr(cls, "roles_users", roles_users)
        return relationship(cls.role_model, secondary=roles_users)

    @property
    def role_manager(self):
        if not hasattr(self, "_role_manager"):
            setattr(self, "_role_manager", RoleManager(self))
        return getattr(self, "_role_manager")

    @property
    def roles(self):
        return self.role_manager

    @roles.setter
    def roles(self, roles):
        # Append roles that are local roles stored in database
        for role in roles:
            self.role_manager.append(role)

    def __str__(self):
        return self.username

    # flask-security wants the user to have a password
    @property
    def password(self):
        return True

    def verify_and_update_password(self, password):
        # Search for the user in the base
        authenticator = current_app.extensions["cfmm"].authenticator
        if authenticator is not None:
            return authenticator.validate_user(self.username, password)
        return False

    def has_role(self, role: t.Union[str, "Role", None]) -> bool:
        if role is None:
            return True
        return super().has_role(role)

    @property
    def is_admin(self):
        return self.has_role(get_role("admin_role"))

    @property
    def has_access(self):
        return self.has_role(get_role("access_role")) or self.is_admin


def get_uuid():
    return str(uuid1())


# noinspection PyAbstractClass
class Permission(Column):
    inherit_cache = True

    def __init__(self, name, type_=None, **kwargs):
        kwargs.setdefault("default", False)
        kwargs.setdefault("server_default", false())
        kwargs.setdefault("nullable", False)
        super().__init__(name, type_ or Boolean(), **kwargs)


@declarative_mixin
class AuthorizationMixin(DateTracking):
    """
    Authorization keys for foreign appliances to execute tasks
    """

    __tablename__ = "authorization"
    user_model = UserMixin

    id = Column("id", Integer(), primary_key=True, nullable=False)
    key = Column("key", String(length=40), default=get_uuid, nullable=False, unique=True)
    source = Column(
        "source", IPNetworkType(), default=ip_network("0.0.0.0/0"), server_default="0.0.0.0/0", nullable=False
    )

    # noinspection PyUnusedLocal
    @validates("source")
    def validate_source(self, key, value):
        return ip_network(value)

    @declared_attr
    def token(cls):
        return Column(
            "token", String(length=40), default=cls.default_token, nullable=cls.default_token() is None, unique=False
        )

    @staticmethod
    def default_token():
        return None

    @declared_attr
    def user_id(cls):
        return Column(
            "user_id",
            Integer(),
            ForeignKey(
                cls.user_model.__tablename__ + ".id",
            ),
            nullable=True,
            index=True,
        )

    @declared_attr
    def user(cls):
        return relationship(cls.user_model.__name__)

    def __str__(self):
        return str(
            {
                c_attr.key: getattr(self, c_attr.key, None)
                for c_attr in inspect(self).mapper.column_attrs
                if c_attr.key != "id"
            }
        )

    # noinspection PyUnusedLocal
    @validates("key")
    def is_valid_uuid(self, key, value):
        """
        Source: https://stackoverflow.com/a/33245493/1168152
        """

        # Throws a ValueError if invalid
        UUID(value, version=1)
        return value

    @classmethod
    def authorize(cls, key, source):
        if key:
            # noinspection PyUnresolvedReferences
            auth = cls.query.filter(cls.key == key).first()
            if not auth:
                raise NotAuthenticated(gettext("Unable to authenticate token {token}").format(token=key))

            if not auth.valid_source(source):
                raise NotAuthenticated(gettext("Invalid source network {address}").format(address=source))

            if auth.invalid_token():
                raise NotAuthenticated(lazy_gettext("Invalid token"))
        else:
            auth = cls()
        return auth

    def valid_source(self, source):
        return ip_address(source) in self.source

    def invalid_token(self):
        # No token provided means it is valid
        if not self.token:
            return False

        return not self._validate_token(request.get_json() or dict())

    def _validate_token(self, data):
        try:
            request_json = base64.b64decode(data["request"])
            token_json = base64.b64decode(data["token"])
        except KeyError:
            return False

        try:
            setattr(self, "_request", json.loads(request_json.decode("utf-8")))
        except (json.JSONDecodeError, TypeError) as err:
            raise BadRequest(gettext("Invalid request: {error}").format(errror=str(err)), loglevel=DEBUG)

        token = self.generate_token(request_json)
        return token == token_json

    def generate_token(self, msg):
        if isinstance(msg, str):
            msg = msg.encode("utf-8")

        return hmac.new(self.token.encode("utf-8"), msg=msg, digestmod=hashlib.sha256).digest()

    def make_request(self, content):
        content_json = json.dumps(content).encode("utf-8")

        token = self.generate_token(content_json)
        return json.dumps(
            {
                "token": base64.b64encode(token).decode("utf-8"),
                "request": base64.b64encode(content_json).decode("utf-8"),
            }
        )

    def get_request(self):
        if not hasattr(self, "_request"):
            raise BadRequest(lazy_gettext("Token protected request not supplied"), loglevel=DEBUG)

        return getattr(self, "_request")

    @classmethod
    def run_task(cls, permissions=None):
        def decorator(task):
            def call(*args, **kwargs):
                # noinspection PyBroadException
                auth = None
                try:
                    auth = cls.authorize(request.headers.get("Authorization", None), request.remote_addr)

                    if not (current_user.is_authenticated and current_user.is_admin):
                        if permissions and not all(getattr(auth, permission, False) for permission in permissions):
                            raise NotAuthorized

                    response = task(*args, auth=auth, **kwargs) or make_response()
                except NotAuthenticated as err:
                    current_app.logger.info(
                        gettext("Unable to authenticate in {task} form {ip}: {msg}").format(
                            task=task.__name__, ip=request.remote_addr, msg=str(err)
                        )
                    )
                    response = make_response(gettext("Not authenticated"), 401)
                except NotAuthorized as err:
                    err.log(
                        gettext("Unauthorized access in {task} from {ip} with {key}: {err}").format(
                            task=task.__name__, ip=request.remote_addr, key=str(auth), err=str(err)
                        )
                    )
                    response = make_response(gettext("Not authorized"), 403)
                except (
                    BadRequest,
                    InvalidUrl,
                    RequestNotFound,
                    NotAcceptable,
                    MethodNotAllowed,
                    LoglevelException,
                ) as err:
                    response = err.response(log=True)
                except Exception as err:
                    current_app.logger.error(
                        gettext("Task Error in {0}: {1}\n{2}").format(task.__name__, str(err), traceback.format_exc())
                    )
                    response = make_response(str(err), 400)
                return response

            return call

        return decorator
