from datetime import timedelta

from flask import request
from flask_admin.form import DateTimeField, widgets as admin_widgets


def disable_field(field):
    render_kw = field.kwargs.pop("render_kw", dict())
    render_kw["disabled"] = True
    field.kwargs["render_kw"] = render_kw
    field.kwargs.pop("validators", None)


class DateTimePickerTimezoneWidget(admin_widgets.DateTimePickerWidget):
    def __call__(self, field, **kwargs):
        kwargs.setdefault("data-timezone-offset", "0")
        if "readonly" in kwargs:
            kwargs.setdefault("data-role", "datetimepicker-readonly")
        return super().__call__(field, **kwargs)


class DateTimeTimezoneField(DateTimeField):
    widget = DateTimePickerTimezoneWidget()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not isinstance(self.format, list):
            self.format = [self.format]

    def process_formdata(self, valuelist):
        super().process_formdata(valuelist)
        if valuelist and self.data and "timezone-offset" in request.form:
            offset = int(request.form["timezone-offset"])
            self.data += timedelta(minutes=-offset)
