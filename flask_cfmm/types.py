from ipaddress import ip_network

import six
from sqlalchemy import types
from sqlalchemy_utils.types.scalar_coercible import ScalarCoercible


# noinspection PyAbstractClass
class IPNetworkType(ScalarCoercible, types.TypeDecorator):
    """
    Changes IPNetwork objects to a string representation on the way in and
    changes them back to IPNetwork objects on the way out.
    """

    impl = types.Unicode(50)
    cache_ok = True

    def __init__(self, max_length: int = 50, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.impl = types.Unicode(max_length)

    def process_bind_param(self, value, dialect):
        return six.text_type(value) if value else None

    def process_result_value(self, value, dialect):
        return ip_network(value) if value else None

    def _coerce(self, value):
        return ip_network(value) if value else None

    @property
    def python_type(self):
        # noinspection PyUnresolvedReferences
        return self.impl.type.python_type
