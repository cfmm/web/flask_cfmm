import requests

from keycloak.uma_permissions import UMAPermission as _UMAPermission

from keycloak.keycloak_uma import (
    KeycloakUMA,
    raise_error_from_response,
    KeycloakPostError,
    KeycloakGetError,
)
from keycloak.openid_connection import KeycloakOpenIDConnection

from keycloak.urls_patterns import URL_CLIENT_UPDATE

from keycloak.keycloak_openid import KeycloakOpenID as _KeycloakOpenID, KeycloakAuthenticationError

__all__ = (KeycloakAuthenticationError, KeycloakOpenIDConnection, KeycloakUMA)


class UMAPermission(_UMAPermission):
    def __init__(self, **kwargs):
        self.resource_id = kwargs.pop("resource_id", None)
        super().__init__(**kwargs)


def get_rpt(response, token):
    auth = dict()
    for data in response.headers.get("WWW-Authenticate", "").split(","):
        if "=" in data:
            key, value = data.split("=", 1)
            auth[key] = value.strip('"')

    with requests.session() as session:
        response = session.get(auth["as_uri"] + "/.well-known/openid-configuration")
        metadata = response.json()
        payload = {
            "grant_type": "urn:ietf:params:oauth:grant-type:uma-ticket",
            "ticket": auth["ticket"],
        }

        headers = {
            "Authorization": "Bearer " + token,
        }
        response = session.post(metadata["token_endpoint"], headers=headers, data=payload)
        return raise_error_from_response(response, KeycloakPostError)


class KeycloakOpenID(_KeycloakOpenID):
    def get_client(self, token: str, client_id: str):
        """Get a client.

        ClientRepresentation:
        https://www.keycloak.org/docs-api/18.0/rest-api/index.html#_clientrepresentation

        :param token: registration access token
        :type token: str
        :param client_id: Keycloak client id
        :return: Client Representation
        :rtype: dict
        """
        params_path = {"realm-name": self.realm_name, "client-id": client_id}
        orig_bearer = self.connection.headers.get("Authorization")
        self.connection.add_param_headers("Authorization", "Bearer " + token)
        data_raw = self.connection.raw_get(URL_CLIENT_UPDATE.format(**params_path))
        (
            self.connection.add_param_headers("Authorization", orig_bearer)
            if orig_bearer is not None
            else self.connection.del_param_headers("Authorization")
        )

        return raise_error_from_response(data_raw, KeycloakGetError)
