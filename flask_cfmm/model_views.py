import uuid
from datetime import date
from functools import cached_property

from flask import redirect, url_for, request, abort, current_app
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    from flask_admin.contrib.sqla import tools
from flask_admin.contrib.sqla.form import AdminModelConverter
from flask_admin.contrib.sqla.view import ModelView as BaseModelView
from flask_admin.contrib.sqla.typefmt import BASE_FORMATTERS
from flask_admin.form.widgets import Select2Widget
from flask_admin.model.form import converts
from flask_security import current_user
from sqlalchemy import and_, func
from wtforms import fields

from .configure import get_role
from .fields import DateTimeTimezoneField
from .filters import CFMMFilterConverter
from .validators import IPNetwork


# noinspection PyAbstractClass
class ModelConverter(AdminModelConverter):
    # noinspection PyUnusedLocal
    @converts("IPNetworkType")
    def convert_date(self, field_args, **extra):
        field_args.setdefault("label", "IP Network")
        field_args["validators"].append(IPNetwork())
        return fields.StringField(**field_args)

    @converts("DateTime")  # includes TIMESTAMP
    def convert_datetime(self, field_args, **extra):
        return DateTimeTimezoneField(**field_args)

    @converts("sqlalchemy.sql.type_api.Variant")
    def convert_variant(self, column, **extra):
        # noinspection PyProtectedMember
        type_name = column.type._type_affinity.__name__
        if type_name in self.converters:
            return self.converters[type_name](column=column, **extra)
        return None


# noinspection PyUnusedLocal
def localtimezone_formatter(view, value, name):
    return current_app.extensions["moment"](value).format("YYYY-MM-DD HH:mm:ssZ")


# Extend flask_admin formatters
BASE_FORMATTERS[date] = localtimezone_formatter


class AuthenticatedAccess(object):
    """
    Class to restrict user access
    """

    def is_accessible(self):
        """
        Only authenticated users can access model view

        :return: boolean
        """
        return current_user.has_access

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def inaccessible_callback(self, name, **kwargs):
        """Redirect unauthenticated requests to login, and authenticated but forbidden requests 403."""
        if current_user.is_authenticated:
            abort(403)
        else:
            return redirect(url_for("security.login", next=request.url))


class ModelView(AuthenticatedAccess, BaseModelView):
    model_form_converter = ModelConverter
    filter_converter = CFMMFilterConverter()

    def get_edit_form(self):
        form = super().get_edit_form()

        add_offset = False
        for field_name in form.__dict__:
            try:
                field = getattr(form, field_name)
                if isinstance(field.field_class.widget, Select2Widget):
                    # Fix for Select2 width
                    render_kw = field.kwargs.pop("render_kw", {})
                    render_kw.setdefault("style", "width: 100%")
                    field.kwargs["render_kw"] = render_kw
                elif field.field_class == DateTimeTimezoneField:
                    add_offset = True
            except AttributeError:
                pass

        if add_offset:
            setattr(
                form, "timezone-offset", fields.HiddenField(render_kw={"data-role": "timezone-offset", "value": "0"})
            )

        return form

    def get_one(self, pk):
        # Support compound primary keys

        # Explicitly cast the values to the appropriate Python type of the SQLA column

        and_clauses = list()
        # noinspection PyProtectedMember
        for a, b in zip(self.model._sa_class_manager.mapper.primary_key, tools.iterdecode(pk)):
            # See documentation of ModelView._cast_string_to_bool for special handling of booleans
            val = ModelView._cast_string_to_bool(b) if a.type.python_type is bool else a.type.python_type(b)
            and_clauses.append(a == val)

        obj = self.get_query().filter(and_(*and_clauses)).one_or_none()

        # super().get_one(pk) uses deprecated Query.get() so use the Session.get() directly instead
        if not obj and self.session.get(self.model, tools.iterdecode(pk)):
            abort(403)

        return obj

    @staticmethod
    def _cast_string_to_bool(val):
        # URL with filter values on boolean columns will result in value of '0' or '1'. This cannot be cast to
        # boolean ( bool('0') and bool('1') evaluates to True ). Cast boolean filter values to integer when
        # parsing the URL arguments to allow unambiguous casting to boolean by the DB driver

        return bool(int(val))

    def _get_list_extra_args(self):
        extra_args = super()._get_list_extra_args()
        if extra_args.filters:
            filters = self.get_filters()
            for flt_index in range(len(extra_args.filters)):
                # See documentation of ModelView._cast_string_to_bool for special handling of booleans
                #
                # Elements of extra_args.filters are a tuple where first element is the SQLA filter object, third
                # element is the value.
                #
                # Tuples are not item-assignable so must be re-constructed.
                if filters[extra_args.filters[flt_index][0]].column.type.python_type is bool:
                    extra_args.filters[flt_index] = extra_args.filters[flt_index][:2] + (
                        ModelView._cast_string_to_bool(extra_args.filters[flt_index][2]),
                    )
        return extra_args


class ModelAdminView(ModelView):
    def is_accessible(self):
        return super().is_accessible() and current_user.is_admin


class RoleView(ModelAdminView):
    pass


class UserView(ModelAdminView):
    column_list = (
        "username",
        "active",
        "last_login_at",
        "current_login_at",
    )
    form_widget_args = {
        "last_login_at": {"readonly": "", "disabled": True},
        "current_login_at": {"readonly": "", "disabled": True},
        "last_login_ip": {"readonly": ""},
        "current_login_ip": {"readonly": ""},
        "login_count": {"readonly": ""},
    }
    column_filters = (
        "active",
        "last_login_at",
        "current_login_at",
    )

    def on_model_change(self, form, model, is_created):
        if is_created:
            if not model.fs_uniquifier:
                model.fs_uniquifier = uuid.uuid4().hex
            if hasattr(self.model, "fs_token_uniquifier") and not model.fs_token_uniquifier:
                model.fs_token_uniquifier = uuid.uuid4().hex


# noinspection PyUnresolvedReferences
class UserOwnedMixin(object):
    required_roles = []

    def on_model_change(self, form, model, is_created):
        if not current_user.is_admin:
            model.user = current_user
        return super().on_model_change(form, model, is_created)

    def is_accessible(self):
        if current_user.is_admin or any(current_user.has_role(role) for role in self.required_roles):
            return super().is_accessible()
        return False

    def get_query(self):
        if current_user.is_admin:
            return self.model.query
        return self.model.query.filter(self.model.user_id == current_user.id)

    def get_count_query(self):
        return self.get_query().with_entities(func.count(self.model.user_id)).order_by(None)


class AuthorizationView(UserOwnedMixin, ModelView):
    form_widget_args = {
        "key": {"style": "width: 300px"},
    }

    column_exclude_list = ["token"]
    form_excluded_columns = ("created", "modified", "user", "token")

    column_searchable_list = ("key", "token")
    column_filters = ("created", "modified", "source", "key", "token")

    @cached_property
    def required_roles(self):
        return [get_role("access_role")]

    def is_accessible(self):
        return super().is_accessible() and not current_user.is_admin


class AuthorizationAdminView(AuthorizationView):
    form_excluded_columns = ("created", "modified")

    @cached_property
    def required_roles(self):
        return [get_role("admin_role")]

    def is_accessible(self):
        return UserOwnedMixin.is_accessible(self)
