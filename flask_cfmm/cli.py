import typing as t
import click
from ipaddress import ip_network

from flask import current_app
from sqlalchemy import inspect
from sqlalchemy.exc import NoResultFound
from werkzeug.local import LocalProxy

from .security import create_user
from .models import Permission

if t.TYPE_CHECKING:
    from .datastore import CFMMUserDatastore

_datastore: "CFMMUserDatastore" = LocalProxy(lambda: current_app.extensions["security"].datastore)


def log_change(changed: bool) -> None:
    if changed:
        print("<<CHANGED>>")


def make_auth(klass, db, **kwargs) -> bool:
    auth = None
    changed = False
    key = kwargs.pop("key")
    user = kwargs.pop("user")
    local = kwargs.pop("local")
    if key is not None:
        try:
            auth = klass.query.filter(klass.key == key).one()
        except NoResultFound:
            kwargs["key"] = key

    if auth is None:
        auth = klass(**kwargs)
        db.session.add(auth)
        changed = True
    else:
        for key, value in kwargs.items():
            setattr(auth, key, value)

    if local:
        auth.source = ip_network("127.0.0.0/8")

    # create_user commits changes, so we need to check for modification before committing
    changed |= db.session.is_modified(auth, include_collections=True)

    if user is not None:
        auth.user = _datastore.find_user(username=user) or create_user(user, active=True)

    changed = db.session.is_modified(auth, include_collections=True)
    db.session.commit()

    return changed


def authorization_factory(bp, klass, db, func=None):
    @click.option("-l", "--local", is_flag=True)
    @click.option("-s", "--source", default="0.0.0.0/0")
    @click.option("-k", "--key")
    @click.option("-t", "--token")
    @click.option("--user")
    def create_authorization(**kwargs):
        f = func or make_auth
        return log_change(f(klass, db, **kwargs))

    inst = inspect(klass)
    for column in inst.c:
        if isinstance(column, Permission):
            create_authorization = click.option(f"--{column.key}", is_flag=True)(create_authorization)

    bp.cli.command("authorization")(create_authorization)
