from flask_sqlalchemy import SQLAlchemy
from flask_sqlalchemy.model import Model as _Model
from sqlalchemy import MetaData, Index, Table, DDL
from sqlalchemy.schema import CreateIndex, UniqueConstraint


class Model(_Model):
    @classmethod
    def build_entities(cls, table, connection, **kwargs):
        # Function to create ReplaceableEntities during registration
        return []

    # noinspection PyUnusedLocal
    @classmethod
    def create_entities(cls, table, connection, **kwargs):
        for entity in cls.build_entities(table, connection, **kwargs):
            ddl = DDL(str(entity.to_sql_statement_create()))
            connection.execute(ddl)

        # Append indices for foreign keys which postgresql does not make by default
        # SQLite and MySQL (InnoDB) create the indices automatically
        if connection.dialect.name in ("postgresql",):
            # noinspection PyUnresolvedReferences
            for fk in table.foreign_key_constraints:
                column = fk.columns[0]
                if (
                    len(fk.columns) == 1
                    and not column.index
                    and not any((column in index.columns.values()) for index in table.indexes)
                    and not any(
                        (isinstance(index, UniqueConstraint) and column.name in index.columns)
                        for index in column.table.constraints
                    )
                ):
                    index = Index(fk.name, column)
                    table.indexes.add(index)

                    CreateIndex(index, if_not_exists=True).execute(connection)

    @classmethod
    def drop_entities(cls, table, connection, **kwargs):
        for entity in reversed(cls.build_entities(table, connection, **kwargs)):
            ddl = DDL(str(entity.to_sql_statement_drop()))
            connection.execute(ddl)

    @classmethod
    def __table_cls__(cls, name, metadata_obj, *arg, **kw):
        listeners = [("after_create", cls.create_entities), ("before_drop", cls.drop_entities)]
        try:
            kw["listeners"] += listeners
        except KeyError:
            kw["listeners"] = listeners

        return Table(name, metadata_obj, *arg, **kw)


def create_database(metadata: MetaData = None):
    metadata = metadata or create_metadata()
    return SQLAlchemy(metadata=metadata, model_class=Model)


def create_metadata(naming_convention=None, schema=None):
    naming_convention = create_naming_convention(naming_convention)
    return MetaData(naming_convention=naming_convention, schema=schema)


def create_naming_convention(naming_convention=None):
    convention = {
        "ix": "ix_%(column_0_N_label)s",
        "uq": "uq_%(table_name)s_%(column_0_N_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s",
    }
    try:
        convention.update(naming_convention)
    except TypeError:
        pass
    return convention
