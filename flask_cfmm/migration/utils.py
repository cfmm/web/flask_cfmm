import logging
from logging.config import fileConfig
from alembic import context
from flask import current_app
from sqlalchemy import event
from sqlalchemy.dialects.sqlite.base import SQLiteDialect
from alembic.operations import ops

config = context.config
fileConfig(config.config_file_name)


def get_engine():
    return current_app.extensions["migrate"].db.engine


def get_engine_url():
    try:
        return get_engine().url.render_as_string(hide_password=False).replace("%", "%%")
    except AttributeError:
        return str(get_engine().url).replace("%", "%%")


config.set_main_option("sqlalchemy.url", get_engine_url())


def get_metadata():
    target_db = current_app.extensions["migrate"].db
    if hasattr(target_db, "metadatas"):
        return target_db.metadatas[None]
    return target_db.metadata


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(url=url, target_metadata=get_metadata(), literal_binds=True)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    logger = logging.getLogger("alembic.env")

    # this callback is used to pre-rpocess revision directives
    # reference: http://alembic.zzzcomputing.com/en/latest/cookbook.html
    # noinspection PyUnusedLocal,PyShadowingNames
    def process_revision_directives(context, revision, directives):
        if getattr(config.cmd_opts, "autogenerate", False):
            script = directives[0]
            # prevent an auto-migration from being generated when there are no changes to the schema
            if script.upgrade_ops.is_empty():
                directives[:] = []
                logger.info("No changes in schema detected.")
            else:
                # Don’t emit DROP INDEX when the table is to be dropped as well
                # reference: https://alembic.sqlalchemy.org/en/latest/cookbook.html#don-t-emit-drop-index-when-the-table-is-to-be-dropped-as-well # noqa: E501
                # process both "def upgrade()", "def downgrade()"
                for directive in (script.upgrade_ops, script.downgrade_ops):
                    # make a set of tables that are being dropped within
                    # the migration function
                    tables_dropped = set()
                    for op in directive.ops:
                        if isinstance(op, ops.DropTableOp):
                            tables_dropped.add((op.table_name, op.schema))

                    # now rewrite the list of "ops" such that DropIndexOp
                    # is removed for those tables.   Needs a recursive function.
                    directive.ops = list(_filter_drop_indexes(directive.ops, tables_dropped))

    def _filter_drop_indexes(directives, tables_dropped):
        # given a set of (tablename, schemaname) to be dropped, filter
        # out DropIndexOp from the list of directives and yield the result.

        for directive in directives:
            # ModifyTableOps is a container of ALTER TABLE types of
            # commands.  process those in place recursively.
            if isinstance(directive, ops.ModifyTableOps) and (directive.table_name, directive.schema) in tables_dropped:
                directive.ops = list(_filter_drop_indexes(directive.ops, tables_dropped))

                # if we emptied out the directives, then skip the
                # container altogether.
                if not directive.ops:
                    continue
            elif isinstance(directive, ops.DropIndexOp) and (directive.table_name, directive.schema) in tables_dropped:
                # we found a target DropIndexOp.   keep looping
                continue

            # otherwise if not filtered, yield out the directive
            yield directive

    connectable = get_engine()

    if isinstance(connectable.dialect, SQLiteDialect):

        @event.listens_for(connectable, "connect")
        def disable_foreign_keys(dbapi_connection, connection_record):
            cursor = dbapi_connection.cursor()
            cursor.execute("PRAGMA foreign_keys=OFF;")
            cursor.close()

    with connectable.connect() as connection:

        context.configure(
            connection=connection,
            target_metadata=get_metadata(),
            process_revision_directives=process_revision_directives,
            **current_app.extensions["migrate"].configure_args,
        )

        with context.begin_transaction():
            context.run_migrations()
