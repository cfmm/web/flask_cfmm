from sqlalchemy import text as sql_text

from alembic_utils.exceptions import SQLParseFailure
from alembic_utils.on_entity_mixin import OnEntityMixin
from alembic_utils.replaceable_entity import ReplaceableEntity, Session, List, T
from alembic_utils.pg_function import parse


class SQLiteTrigger(OnEntityMixin, ReplaceableEntity):
    """A SQLite Trigger compatible with `alembic revision --autogenerate`

    **Parameters:**

    * **schema** - *str*: A SQL schema name
    * **signature** - *str*: A SQL trigger's call signature
    * **definition** - *str*:  The remaining trigger body and identifiers
    * **on_entity** - *str*:  fully qualified entity that the policy applies

    SQLite Create Trigger Specification:

        CREATE TRIGGER [IF NOT EXISTS] trigger_name
        [BEFORE|AFTER|INSTEAD OF] [INSERT|UPDATE|DELETE]
        ON table_name
        [WHEN condition]
        BEGIN
            statements;
        END;
    """

    type_ = "trigger"

    _templates = [
        "create{:s}trigger{:s}{signature}{:s}{event}{:s}ON{:s}{on_entity}{:s}{action}",
    ]

    def __init__(
        self,
        schema: str,
        signature: str,
        definition: str,
        on_entity: str,
    ):
        if "." not in on_entity:
            on_entity = "main." + on_entity

        super().__init__(schema=schema, signature=signature, definition=definition, on_entity=on_entity)  # type: ignore

    def render_self_for_migration(self, omit_definition=False) -> str:
        """Render a string that is valid python code to reconstruct self in a migration"""
        var_name = self.to_variable_name()
        class_name = self.__class__.__name__
        escaped_definition = self.definition if not omit_definition else "# not required for op"

        return f"""{var_name} = {class_name}(
    schema="{self.schema}",
    signature="{self.signature}",
    on_entity="{self.on_entity}",
    definition={repr(escaped_definition)}
)\n"""

    @classmethod
    def from_sql(cls, sql: str) -> "SQLiteTrigger":
        """Create an instance from a SQL string"""
        for template in cls._templates:
            result = parse(template, sql, case_sensitive=False)
            if result is not None:
                # remove possible quotes from signature
                signature = result["signature"]
                event = result["event"]
                on_entity = result["on_entity"]
                action = result["action"]

                if "." not in on_entity:
                    on_entity = "main" + "." + on_entity

                schema = on_entity.split(".")[0]

                definition_template = " {event} ON {on_entity} {action}"
                definition = definition_template.format(event=event, on_entity=on_entity, action=action)

                return cls(
                    schema=schema,
                    signature=signature,
                    on_entity=on_entity,
                    definition=definition,
                )
        raise SQLParseFailure(f'Failed to parse SQL into SQLiteTrigger """{sql}"""')

    def to_sql_statement_create(self):
        """Generates a SQL "create trigger" statement for SQLiteTrigger"""

        # We need to parse and replace the schema qualifier on the table for simulate_entity to
        # operate
        _def = self.definition
        _template = "{event}{:s}ON{:s}{on_entity}{:s}{action}"
        match = parse(_template, _def)
        if not match:
            raise SQLParseFailure(f'Failed to parse SQL into SQLiteTrigger.definition """{_def}"""')

        event = match["event"]
        action = match["action"]

        # Ensure entity is qualified with schema
        on_entity = match["on_entity"]
        if "." in on_entity:
            _, _, on_entity = on_entity.partition(".")
        on_entity = f"{self.schema}.{on_entity}"

        # Re-render the definition ensuring the table is qualified with
        def_rendered = _template.replace("{:s}", " ").format(event=event, on_entity=on_entity, action=action)

        return sql_text(f'CREATE TRIGGER "{self.signature}" {def_rendered}')

    def to_sql_statement_drop(self, exists=True, **kwargs):
        """Generates a SQL "drop trigger" statement for SQLiteTrigger"""
        existence = "IF EXISTS" if exists else ""
        return sql_text(f'DROP TRIGGER {existence} "{self.signature}";')

    def to_sql_statement_create_or_replace(self):
        """Generates a SQL "replace trigger" statement for SQLiteTrigger"""
        yield sql_text(f'DROP TRIGGER IF EXISTS "{self.signature}";')
        yield self.to_sql_statement_create()

    @classmethod
    def from_database(cls, sess: Session, schema="%") -> List[T]:
        """Get a list of all triggers defined in the db"""

        if sess.bind.dialect.name != "sqlite":
            return []

        # noinspection SqlDialectInspection,SqlNoDataSourceInspection
        sql = sql_text(
            """
            SELECT name, tbl_name, `sql` FROM sqlite_master WHERE type = 'trigger';
        """
        )
        rows = sess.execute(sql, {"schema": schema}).fetchall()

        db_triggers = [cls.from_sql(x[2]) for x in rows]

        for trig in db_triggers:
            assert trig is not None

        return db_triggers
