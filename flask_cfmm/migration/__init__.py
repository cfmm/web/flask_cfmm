import typing

from alembic_utils import replaceable_entity

# noinspection PyUnresolvedReferences
from sqlalchemy.orm import mapperlib

from .sqllite_trigger import SQLiteTrigger

if typing.TYPE_CHECKING:
    from sqlalchemy.engine import Engine


def register_entities(connection: "Engine") -> None:
    # noinspection PyProtectedMember
    if connection.dialect.name == "sqlite":
        entity_types = {SQLiteTrigger}
    else:
        entity_types = None

    for mapper_registry in mapperlib._all_registries():
        for mapper in mapper_registry.mappers:
            try:
                replaceable_entity.register_entities(
                    mapper.class_.build_entities(mapper.class_.__table__, connection), entity_types=entity_types
                )
            except (TypeError, AttributeError):
                pass
