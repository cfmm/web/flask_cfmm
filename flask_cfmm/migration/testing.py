# noinspection PyPackageRequirements
import pytest
import flask_migrate


class NotEmpty(Exception):
    pass


def check_migrate(
    app,
    directory=None,
    message=None,
    sql=False,
    head="head",
    splice=False,
    branch_label=None,
    version_path=None,
    rev_id=None,
    x_arg=None,
):
    # Check that no migration is outstanding

    config = app.extensions["migrate"].migrate.get_config(directory, x_arg=x_arg)
    config.set_main_option("revision_environment", "true")

    # noinspection PyUnusedLocal
    def process_revision_directives(context, revision, directives):
        if not directives[0].upgrade_ops.is_empty():
            raise ValueError("Migration not empty")
        directives[:] = []

    return flask_migrate.command.revision(
        config,
        message,
        autogenerate=True,
        sql=sql,
        head=head,
        splice=splice,
        branch_label=branch_label,
        version_path=version_path,
        rev_id=rev_id,
        process_revision_directives=process_revision_directives,
    )


def check_migrations(app, revision=None):
    db = app.extensions["sqlalchemy"]
    with app.app_context():
        flask_migrate.upgrade()
        db.close_all_sessions()
        flask_migrate.downgrade(revision=revision or "base")
        flask_migrate.upgrade()

        err = None
        try:
            check_migrate(app)
        except NotEmpty as e:
            err = e

        # Do not call pytest.fail in exception handler as that generates verbose, cascading exception output
        if err is not None:
            pytest.fail(f"Raised exception: {err}")
