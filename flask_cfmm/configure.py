import datetime
import json
import os
import random
import string
from pathlib import Path

import yaml
from flask import Flask, current_app
from flask_security.utils import uia_username_mapper


def load_configuration(app: Flask, config: str = None) -> None:
    if config is None:
        config = os.environ.get("WEB_APP_CONFIG")

    if config:
        config = Path(config)
        if not config.is_absolute():
            config = Path.cwd().joinpath(config)
        loaded = False
        suffix = config.suffix.lower()
        try:
            if suffix in (".py",):
                loaded = app.config.from_pyfile(config, silent=True)
        except (NameError, SyntaxError):
            pass
        if not loaded:
            # noinspection PyUnresolvedReferences
            try:
                if suffix in (".yml", ".yaml"):
                    loaded = app.config.from_file(config, yaml.safe_load, silent=True)
            except yaml.scanner.ScannerError:
                pass
        if not loaded:
            try:
                if suffix in (".json",):
                    # noinspection PyUnusedLocal
                    loaded = app.config.from_file(config, json.load, silent=True)
            except json.decoder.JSONDecodeError:
                pass


def configure_defaults(app: Flask) -> None:
    # Default session is 31 days of OIDC
    timeout = app.config.get("SESSION_TIMEOUT", 44640)
    app.config["PERMANENT_SESSION_LIFETIME"] = datetime.timedelta(minutes=timeout)

    app.config.setdefault("SECURITY_PASSWORD_SALT", "NOT USED DOES NOT MATTER")
    app.config.setdefault("SECURITY_PASSWORD_HASH", "pbkdf2_sha512")
    app.config["SECRET_KEY"] = app.config.get("SECRET_KEY", None) or "".join(
        random.choice(string.ascii_uppercase) for _ in range(64)
    )

    app.config.setdefault("COLLECT_STATIC_ROOT", str(Path(app.root_path).parent.joinpath("static")))
    app.config.setdefault("STATIC_URL_PATH", "/static")
    app.config.setdefault("CFMM_TOKEN_VALIDATE_INTERVAL", 30)

    app.config["SECURITY_TRACKABLE"] = True
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = app.config.get("SQLALCHEMY_TRACK_MODIFICATIONS", False) or False
    app.config.setdefault("SECURITY_REGISTERABLE", False)
    app.config.setdefault("SECURITY_SEND_REGISTER_EMAIL", False)
    app.config.setdefault("SECURITY_USERNAME_ENABLE", True)
    app.config.setdefault("SECURITY_JOIN_USER_ROLES", False)
    app.config.setdefault(
        "SECURITY_USER_IDENTITY_ATTRIBUTES",
        [{"username": {"mapper": uia_username_mapper, "case_insensitive": True}}],
    )
    app.config.setdefault("SQLALCHEMY_ECHO", False)
    app.config.setdefault("SQLALCHEMY_DATABASE_URI", os.environ.get("DATABASE_URL", "sqlite:///:memory:"))
    app.config.setdefault("TITLE", "CFMM Flask App")

    app.config.setdefault("KEYCLOAK_CLIENT_ID", "cfmm_flask")
    app.config.setdefault("KEYCLOAK_CLIENT_SECRET", None)

    app.config.setdefault("FLASK_ADMIN_FLUID_LAYOUT", True)

    app.config.setdefault("CFMM_ADMIN_ROLE", "admin")
    app.config.setdefault("CFMM_AUTH_ROLE", "auth")
    app.config.setdefault("CFMM_ACCESS_ROLE", None)


def config_value(key, app=None, default=None, strict=True):
    app = app or current_app
    key = f"CFMM_{key.upper()}"
    # protect against spelling mistakes
    if strict and key not in app.config:
        raise ValueError(f"Key {key} doesn't exist")
    return app.config.get(key, default)


def get_role(key):
    value = config_value(key)
    if value is not None:
        return value.lower()
