from filecmp import cmp
from os import makedirs, walk, rmdir
from os import path as op
from os import remove
from shutil import copy

from flask_collect import Collect as CollectBase
from flask_collect.storage.file import Storage as BaseStorage


class Collect(CollectBase):
    def init_app(self, app):
        super().init_app(app)

        if hasattr(app, "cli"):
            import click

            @app.cli.command()
            @click.option("--idempotent", is_flag=True)
            @click.option("--cleanup/--no-cleanup", default=True, is_flag=True)
            def collect(idempotent, cleanup):  # noqa
                if idempotent:
                    state = app.extensions["collect"]
                    state.storage = "flask_cfmm.collect"
                    state.cleanup = cleanup
                app.extensions["collect"].collect(verbose=True)


class Storage(BaseStorage):
    """Storage that copies static files."""

    def run(self):
        """Collect static files from blueprints."""
        self.log("Collect static from blueprints.")
        changed = False
        cleanup = getattr(self.collect, "cleanup", True)
        paths = list()
        for bp, f, o in self:
            destination = op.join(self.collect.static_root, o)

            destination_dir = op.dirname(destination)
            if not op.exists(destination_dir):
                makedirs(destination_dir)

            if cleanup:
                paths.append(destination)

            if op.exists(destination):
                if op.getmtime(destination) >= op.getmtime(f):
                    continue

                # Skip files that are the same, except of the timestamp
                if cmp(destination, f, shallow=False):
                    continue

                remove(destination)

            copy(f, destination)
            changed = True
            self.log("Copied: [{0}] '{1}'".format(bp.name, op.join(self.collect.static_url, destination)))

        if cleanup:
            for root, dirs, files in walk(self.collect.static_root, topdown=False):
                for file in files:
                    path = op.join(root, file)
                    if path not in paths:
                        remove(path)
                        changed = True
                        self.log(f"Removed file: {path}")

                for file in dirs:
                    path = op.join(root, file)
                    try:
                        rmdir(path)
                        changed = True
                        self.log(f"Removed empty directory: {path}")
                    except (FileNotFoundError, OSError):
                        pass

        if changed:
            print("<<<CHANGED>>>")
