import typing as t

from flask import current_app
from flask_principal import identity_loaded
from flask_security import Security
from flask_security.core import AnonymousUser as AnonymousUserBase

# noinspection PyProtectedMember
from flask_security.core import _context_processor

# noinspection PyProtectedMember
from flask_security.core import _on_identity_loaded
from werkzeug.local import LocalProxy

from .configure import get_role

if t.TYPE_CHECKING:
    from .datastore import CFMMUserDatastore
    from flask import Flask

_datastore: "CFMMUserDatastore" = LocalProxy(lambda: current_app.extensions["security"].datastore)


class AnonymousUser(AnonymousUserBase):
    @property
    def is_admin(self):
        return False

    @property
    def has_access(self):
        return False


class CFMMSecurity(Security):
    # noinspection PyUnresolvedReferences
    def init_app(
        self,
        app: "Flask",
        datastore: "CFMMUserDatastore" = None,
        register_blueprint: t.Optional[bool] = None,
        **kwargs: t.Any,
    ) -> None:
        # Flask-Security-Too adds username field to form class and throws and error if the attribute already exists
        # However in testing, the app is created multiple times and the username field will exist after the first
        # initialization of the app. Because we know that the username attribute does not exist in our custom form
        # classes, we can safely delete the attribute. In production the app is only initialized once, so this really
        # only affects testing.
        for f in (
            "register_form",
            "confirm_register_form",
            "login_form",
        ):
            if hasattr(self.forms[f].cls, "username"):
                del self.forms[f].cls.username

        super().init_app(app, datastore, register_blueprint=False)
        self.login_manager.anonymous_user = AnonymousUser
        bp = datastore.setup_oidc(app, self)
        app.register_blueprint(bp)
        app.context_processor(_context_processor)

        # Flask-Principal is a poorly designed resource hog
        # The roles of the current user are enumerated (an expensive operation) on every call, even if they are never
        # used, e.g. retrieving static content. Therefore, we break the connection. This will disable the unused
        # functionality related to roles. However, flask-admin does not use the flask-principal but rather
        # is_accessible methods which can do the role check JIT.
        identity_loaded.disconnect(_on_identity_loaded, app)


def create_user(username, **kwargs):
    user = _datastore.create_user(username=username, **kwargs)
    # First user to login without an admin role is made admin user.
    admin_role = get_role("admin_role")
    if admin_role and not user.has_role(admin_role) and _datastore.find_role(admin_role) is None:
        _datastore.add_role_to_user(user, _datastore.create_role(name=admin_role))
    _datastore.commit()
    return user


def add_roles(roles):
    for role in roles:
        _datastore.find_or_create_role(role)
    _datastore.commit()


class Authenticator(object):
    def validate_user(self, username: str, password: str) -> bool:
        return False


class DictionaryAuthenticator(Authenticator):
    def __init__(self, users):
        self.users = users

    def validate_user(self, username, password):
        if username in self.users:
            return self.users[username] == password
        return False
