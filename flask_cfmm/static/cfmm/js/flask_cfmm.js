/* Display the loading screen on form submission */
// noinspection JSUnresolvedFunction

$(function () {

    // on row click
    $("table.model-list").on("click", "tr", function (e) {
        // don't redirect if clicked on a link or form
        // don't redirect if clicked on the first column, which contains the action checkbox
        if ($(e.target).is("a, input, span") || e.target.cellIndex === 0)
            return;

        // don't redirect if clicked inside a column containing a link, but not on the link itself
        if ($(e.target).is('td') && $(e.target).find('a').length > 0)
            return;

        // don't redirect if text is selected
        let selection;
        if (window.getSelection) {
            selection = window.getSelection().toString();
        }
        else if (document.selection && document.selection.type != "Control") {
            selection = document.selection.createRange().text;
        }
        if (selection)
            return;

        // otherwise, redirect to edit page for this row's record
        let location_href = $(this).find("td[class='list-buttons-column'] a[title*='Edit']").attr('href');
        if (location_href)
            location.href = location_href;
    });

    // on table row hover, make cursor a pointer
    // noinspection JSUnusedLocalSymbols
    $("table.model-list tbody").on("mouseenter mouseleave", function (e) {
        $("table.model-list tbody").css('cursor', 'pointer');
    });

    // noinspection JSUnusedLocalSymbols
    $("form").on("submit", function (event) {
        if (document.activeElement.classList.contains("no-load")) {
            return true;
        }

        // use the loading-screen element if it's included
        let load_screen = $("#loading-screen");
        if (load_screen.length) {
            // Make the root div element translucent and not interactive
            $(".container").attr("style", "opacity: 0.5; pointer-events: none;");
            $(".container-fluid").attr("style", "opacity: 0.5; pointer-events: none;");
            // Make the loading element visible and place it in the centre of the screen
            load_screen.attr("style", "display: flex;");
        }
        return true;
    });
});

// noinspection JSUnusedLocalSymbols
$(window).on("pageshow", function (event) {
    // Disable the loading-screen when the page unloads
    let load_screen = $("#loading-screen");
    if (load_screen.length) {
        $(".container").attr("style", "opacity: 1.0; pointer-events: auto;");
        $(".container-fluid").attr("style", "opacity: 1.0; pointer-events: auto;");
        load_screen.attr("style", "display: none;");
    }
});

$(document).on('adminFormReady', function () {
    // change date inputs' year select element to be 1900-current_year
    // Only select year
    $(':input[data-role], a[data-role]').each(function () {
        let $el = $(this);
        switch ($el.attr('data-role')) {
            case 'datetimepicker':
            case 'datetimepicker-readonly':
                const offset = moment().utcOffset();
                const valueOffset = parseInt($el.attr('data-timezone-offset'));
                if (!isNaN(valueOffset) && offset !== valueOffset) {
                    const format = $el.attr('data-date-format');
                    let date = moment($el.val(), format);
                    if (date.isValid()) {
                        date.add(offset - valueOffset, 'minutes');
                        $el.val(date.format(format));
                        $el.attr('data-timezone-offset', offset);
                    }
                }
                return true;
            case 'timezone-offset':
                $el.val(moment().utcOffset());
                return true;
        }
    });
});
