import datetime
import time
import typing as t
from urllib.parse import urlencode, urlparse, urljoin

import requests
from authlib.common.errors import AuthlibBaseError
from authlib.integrations.base_client.errors import MismatchingStateError
from authlib.integrations.flask_client import OAuth, OAuthError, FlaskOAuth2App
from authlib.jose import JsonWebKey, JsonWebToken
from authlib.jose.errors import DecodeError, InvalidClaimError, ExpiredTokenError
from flask import url_for, current_app, redirect, request, abort, flash, session, make_response
from flask_babel import gettext
from flask_security import current_user, login_user, RoleMixin
from flask_security.utils import config_value as cv

# noinspection PyProtectedMember
from flask_security.core import _user_loader as _user_loader_security
from flask_security.datastore import SQLAlchemyUserDatastore
from flask_security.utils import set_request_attr, config_value
from flask_security.views import logout, create_blueprint
from requests.exceptions import HTTPError
from sqlalchemy import orm

# noinspection PyProtectedMember
from .security import create_user, _datastore
from .exceptions import RedirectException

if t.TYPE_CHECKING:
    from flask_security.datastore import Role


class TokenInvalid(Exception):
    pass


class DisableTokenRenewal(object):
    def __init__(self, user):
        self.user = user

    def __enter__(self):
        if self.user.is_authenticated:
            self.user.token.renewable = False
        return self.user

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.user.is_authenticated:
            self.user.token.renewable = True


def is_safe_url(target):
    """
    Ensure redirect point to the same server.

    :param target: URL to check
    :type target: string
    :returns: string
    :raises: None
    """
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc


def get_redirect_target(default=None):
    """
    Find a valid redirect target in a list of hints

    :returns: string, None
    :raises: None
    """
    for target in request.args.get("next"), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target
    return default


class OIDCRole(RoleMixin):
    def __init__(self, name, role_id=None, description=None, permissions=None):
        self.name = name
        self.id = role_id
        self.description = description
        self.permissions = permissions or set()
        super().__init__()

    def __str__(self):
        return str(self.name)


class OIDCClient(FlaskOAuth2App):
    def wrap_for_stale(self, callback):
        try:
            result = callback()
        except InvalidClaimError:
            # Possibly stale metadata
            load_time = self.load_server_metadata().pop("_loaded_at", None)
            # Force metadata reload just in case authlib directly accesses the dictionary without calling
            # load_server_metadata
            self.load_server_metadata()

            # Try again
            result = callback()

            # Log that stale metadata was the problem
            try:
                date_time = datetime.datetime.fromtimestamp(load_time, datetime.timezone.utc)
            except TypeError:
                date_time = gettext("Unknown")
            current_app.logger.info(gettext("Stale server metadata from {time}").format(time=date_time))

        return result

    def introspect(self, token_string, force=False):
        def func():
            try:
                if force:
                    raise DecodeError
                return self.parse_access_token(token_string)
            except (DecodeError, InvalidClaimError, ExpiredTokenError):
                claims = self._introspect(token_string)
                if not claims.get("active", False):
                    raise TokenInvalid
                return claims

        return self.wrap_for_stale(func)

    def _introspect(self, token_string):
        url = self.load_server_metadata().get("introspection_endpoint", None)
        if url:
            data = {"token": token_string, "token_type_hint": "access_token"}
            auth = (self.client_id, self.client_secret)
            resp = requests.post(url, data=data, auth=auth)
            resp.raise_for_status()
            return resp.json()
        return {}

    def load_key(self, header, _):
        jwk_set = JsonWebKey.import_key_set(self.fetch_jwk_set())

        try:
            return jwk_set.find_by_kid(header.get("kid"))
        except ValueError:
            # re-try with new jwk set
            jwk_set = JsonWebKey.import_key_set(self.fetch_jwk_set(force=True))
            return jwk_set.find_by_kid(header.get("kid"))

    def parse_access_token(self, token_string, issuer=None):
        metadata = self.load_server_metadata()
        claims_options = None
        issuers = list()
        if "issuer" in metadata:
            issuers.append(metadata["issuer"])
        if issuer is not None:
            issuers.append(issuer)
        if len(issuers) > 0:
            claims_options = {"iss": {"values": issuers}}

        alg_values = metadata.get("id_token_signing_alg_values_supported") or ["RS256"]
        jwt = JsonWebToken(alg_values)
        claims = jwt.decode(
            token_string,
            key=self.load_key,
            claims_options=claims_options,
            claims_params=dict(client_id=self.client_id),
        )
        claims.validate(leeway=120)
        return claims

    def parse_logout_token(self, token_string):
        def func():
            # https://openid.net/specs/openid-connect-backchannel-1_0.html#Validation
            metadata = self.load_server_metadata()
            claims_options = {
                "aud": {"values": [self.client_id]},
                "events": {"validate": lambda a, b: "http://schemas.openid.net/event/backchannel-logout" in b},
                "nonce": {"value": None},
            }
            if "issuer" in metadata:
                claims_options["iss"] = {"values": [metadata["issuer"]]}
            alg_values = metadata.get("id_token_signing_alg_values_supported") or ["RS256"]
            claim = JsonWebToken(algorithms=alg_values).decode(
                token_string,
                key=self.load_key,
                claims_options=claims_options,
            )
            claim.validate(leeway=120)
            return claim

        return self.wrap_for_stale(func)

    def authorize_access_token(self, **kwargs):
        # super() does not work in the local func
        parent_func = super().authorize_access_token

        def func():
            return parent_func(**kwargs)

        return self.wrap_for_stale(func)


class CFMMUserDatastore(SQLAlchemyUserDatastore):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.token_cls = None
        self.client = None

    def setup_oidc(self, app, security):
        bp = create_blueprint(app, security, __name__)
        meta_url = app.config.get("KEYCLOAK_META_URL")
        assert meta_url is not None, "KEYCLOAK_META_URL must be set"

        self.token_cls = _generate_token(self)
        oauth = OAuth(app, fetch_token=self.fetch_token, update_token=self.update_token)
        self.client = oauth.register(
            "keycloak",
            server_metadata_url=meta_url,
            client_kwargs={"scope": "openid profile email roles"},
            client_cls=OIDCClient,
        )

        # https://flask.palletsprojects.com/en/2.1.x/signals/?highlight=signals#sending-signals
        # "Never pass current_app as sender to a signal."
        # authlib uses current_app as sender, so signals are broken.
        # token_update.connect_via(app)(self.update_token)

        security.login_manager.login_view = bp.name + ".login"
        security.login_manager.user_loader(self._user_loader)

        # Remove the previous login and logout views
        logout_methods = cv("LOGOUT_METHODS", app=app)
        if logout_methods is not None:
            del bp.deferred_functions[0]
            # noinspection PyUnresolvedReferences
            bp.route(config_value("LOGOUT_URL", app=app), methods=logout_methods, endpoint="logout")(self.logout)
        del bp.deferred_functions[0]
        # noinspection PyUnresolvedReferences
        bp.route(config_value("LOGIN_URL", app=app), methods=["GET", "POST"], endpoint="login")(self.login)

        bp.route("/authorize", methods=["GET", "POST"], endpoint="authorize")(self.authorize)
        bp.route("/deauthorize", methods=["GET", "POST"], endpoint="deauthorize")(self.deauthorize)

        bp.route("/oidc/logout", methods=["POST"], endpoint="logout_oidc_session")(
            app.extensions["cfmm"].csrf.exempt(self.session_logout)
        )
        bp.route("/account", methods=["GET"], endpoint="account")(self.account)
        bp.route("/landing", methods=["GET"], endpoint="landing")(self.landing)

        # noinspection PyUnusedLocal
        @app.errorhandler(TokenInvalid)
        def token_invalid(response):
            return logout()

        return bp

    @staticmethod
    def landing():
        return redirect(get_redirect_target("/"))

    def account(self):
        metadata = self.client.load_server_metadata()
        issuer = metadata.get("issuer")
        # This is keycloak specific
        return redirect(f"{issuer}/account")

    def _user_loader(self, user_id):
        user = _user_loader_security(user_id)
        if user is not None:
            user.token = self.token_cls.find(name=self.client.name, user=user, force=False)
            if user.token is None:
                set_request_attr("fs_authn_via", None)
                return None
        return user

    def find_role(self, role: str) -> t.Union["Role", None]:
        # Check if the Role is provided via OIDC
        token = getattr(current_user, "token", None)
        if token and str(role).lower() in token.roles:
            return OIDCRole(name=role)

        return super().find_role(role)

    # noinspection PyUnusedLocal
    @staticmethod
    def fetch_token(name):
        token = getattr(current_user, "token", None)
        return token.to_token() if token else None

    # noinspection PyUnusedLocal
    @staticmethod
    def update_token(name, token, refresh_token=None, access_token=None):
        item = getattr(current_user, "token", None)
        if item:
            item.update(token)

    def login(self, redirect_url=None):
        params = dict()
        redirect_uri = get_redirect_target(redirect_url)

        if current_user.is_authenticated:
            return redirect(redirect_uri)

        if redirect_uri:
            params.setdefault("next", redirect_uri)

        redirect_uri = url_for("security.authorize", _external=True, **params)
        response = self.client.authorize_redirect(redirect_uri)
        return response

    def get_user(self, userinfo, create=False):
        if userinfo is None:
            return None

        username = userinfo.get("preferred_username", None) or userinfo.get("email")
        user_id = None
        user = None
        # providers have different unique keys
        # keycloak uses 'sub'
        for x in ("sub",):
            if x in userinfo:
                user_id = userinfo.get(x)
                break
        if user_id:
            user = self.find_user(fs_uniquifier=user_id)
        if not user and username:
            user = self.find_user(username=username, case_insensitive=True)
            if user and user_id:
                user.fs_uniquifier = user_id

        if create and not user and username and user_id:
            user = create_user(username, fs_uniquifier=user_id)

        return user

    def authorize(self):
        try:
            token = self.client.authorize_access_token()
        except (MismatchingStateError, InvalidClaimError) as err:
            flash(gettext("Token could not be validated"))
            current_app.logger.info(gettext("Token validation error: {error}").format(error=str(err)))
            return redirect("/")
        except AuthlibBaseError as err:
            flash(gettext("Token could not be validated: {error}").format(error=str(err)))
            current_app.logger.error(gettext("Unrecognized token validation error: {error}").format(error=str(err)))
            return abort(403)

        # Lookup the user
        userinfo = token.get("userinfo", None) or self.client.userinfo()
        user = self.get_user(userinfo, create=True)

        # Store the OIDC session id with the token
        token["sid"] = userinfo.get("sid", None)

        if user:
            login_user(user, authn_via="oidc")
            self.commit()
            user.token = self.token_cls.find(name=self.client.name, user=user)
            user.token.update(token)

        return redirect(get_redirect_target("/"))

    def logout(self):
        token = getattr(current_user, "token", None)
        if token is None:
            return logout()

        metadata = self.client.load_server_metadata()
        deauthorization_endpoint = metadata.get("end_session_endpoint")
        if deauthorization_endpoint:
            redirect_url = url_for(".deauthorize", _external=True)
            data = {
                "post_logout_redirect_uri": redirect_url,
            }
            if token.id_token:
                data["id_token_hint"] = token.id_token

            # Keycloak does not necessarily redirect back for deauthorization
            logout()

            query = urlencode(data)
            url = f"{deauthorization_endpoint}?{query}"
            response = redirect(url, code=307)
            return response

        return logout()

    @staticmethod
    def deauthorize():
        return logout()

    def session_logout(self):
        # https://openid.net/specs/openid-connect-backchannel-1_0.html#LogoutToken
        # iss - Issuer Identifier - required
        # sub - Subject Identifier - optional
        # aud - Audience - required
        # iat - Issued At Time - required
        # jti - JWT ID - required
        # events - empty http://schemas.openid.net/event/backchannel-logout - required
        # sid - Session ID - optional

        logout_token = request.form["logout_token"]

        try:
            claims = self.client.parse_logout_token(logout_token)
        except AuthlibBaseError as err:
            current_app.logger.error(gettext("Unrecognized token validation error: {error}").format(error=str(err)))
            return abort(400)

        sub = claims.get("sub", None)
        sid = claims.get("sid", None)

        # At least one of sid or sub must be defined
        if not sub and not sid:
            abort(400)

        # Remove all records for sub and sid
        query = self.token_cls.query.filter(self.token_cls.name == self.client.name)
        if sub:
            query = query.join(self.token_cls.user).filter(self.user_model.fs_uniquifier == sub)
        if sid:
            query = query.filter(self.token_cls.sid == sid)
        for record in query.all():
            self.db.session.delete(record)

        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            current_app.logger.error(gettext("Failed to commit token removal: {err}").format(err=str(e)))
            abort(400)

        # tokens that expired more than the app session lifetime ago should be purged
        expires_at = current_app.config["PERMANENT_SESSION_LIFETIME"]
        if isinstance(expires_at, datetime.timedelta):
            expires_at = expires_at.total_seconds()
        expires_at = time.time() - expires_at
        self.token_cls.query.filter().filter(self.token_cls.expires_at <= expires_at).delete(synchronize_session=False)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            current_app.logger.error(gettext("Failed to commit expired token removal: {err}").format(err=str(e)))

        response = make_response(gettext("Logout completed"), 200)
        response.cache_control.no_cache = True
        response.cache_control.no_store = True
        response.headers["Pragma"] = "no-cache"
        return response


def _generate_token(datastore):
    db = datastore.db

    for mapper in db.Model.registry.mappers:
        if "<class 'flask_cfmm.datastore._generate_token.<locals>.OAuth2Token'>" == str(mapper.class_):
            return mapper.class_

    class OAuth2Token(db.Model):
        __tablename__ = "oauth2token"

        __table_args__ = {
            "mysql_collate": "utf8_general_ci",
            "mysql_charset": "utf8",
        }

        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(length=256), nullable=False, index=True)
        token_type = db.Column(db.String(length=40))
        access_token = db.Column(db.String(length=4096))
        refresh_token = db.Column(db.String(length=4096))
        id_token = db.Column(db.String(length=4096))
        expires_at = db.Column(db.Integer())
        sid = db.Column(db.String(length=256), index=True)
        session = db.Column(db.String(length=256), index=True)

        user_id = db.Column(
            db.Integer(),
            db.ForeignKey(
                f"{datastore.user_model.__tablename__}.id",
                ondelete="CASCADE",
                onupdate="CASCADE",
            ),
            nullable=False,
            index=True,
        )

        user = datastore.db.relationship(datastore.user_model)

        def __init__(self, *args, **kwargs):
            self._roles = None
            self.renewable = kwargs.pop("renewable", True)
            super().__init__(*args, **kwargs)

        @orm.reconstructor
        def init_on_load(self):
            self.renewable = True

        def to_token(self):
            return dict(
                access_token=self.access_token,
                token_type=self.token_type,
                refresh_token=self.refresh_token,
                expires_at=self.expires_at,
                id_token=self.id_token,
            )

        def invalidate(self):
            self.access_token = None
            self.refresh_token = None
            self.expires_at = time.time()
            self._roles = None
            self.id_token = None
            self.save()

        def renew_login(self):
            self.invalidate()
            logout()
            raise RedirectException(_datastore.login(redirect_url=request.url))

        def renew(self):
            # Return immediately if token has not expired with a 2-second buffer
            if not self.renewable or (self.expires_at and self.expires_at - 2 > time.time()):
                return

            try:
                metadata = datastore.client.load_server_metadata()
                url = metadata.get("token_endpoint")
                # noinspection PyProtectedMember
                with datastore.client._get_oauth_client(**metadata) as client:
                    if self.refresh_token and url:
                        token = client.refresh_token(url, refresh_token=self.refresh_token)
                    elif metadata.get("grant_type") == "client_credentials":
                        token = client.fetch_token(url, grant_type="client_credentials")
                    else:
                        raise TokenInvalid(gettext("No refresh_token"))
                self.update(token)
            except (TokenInvalid, OAuthError, HTTPError, AuthlibBaseError) as err:
                current_app.logger.warning(
                    gettext("Token renew failed for {user}: {err}").format(user=self.user.username, err=str(err))
                )
                self.renew_login()

        def update(self, token):
            if token:
                self.token_type = token["token_type"]
                self.access_token = token["access_token"]
                self.refresh_token = token.get("refresh_token", None)
                self.expires_at = token.get("expires_at", None)
                self.id_token = token.get("id_token", self.id_token)
                self.sid = token.get("sid", self.sid)
                self.session = session.get("_id", self.session)
                self._roles = None
                self.save()
                self.validated()

        def populate(self):
            claims = dict()
            if self.access_token:
                self.renew()
                try:
                    claims = datastore.client.introspect(
                        self.access_token, force=(session.get("token_validate", 0) > time.time())
                    )
                    self.validated()
                except TokenInvalid:
                    pass
            self._roles = set(
                OIDCRole(name=role.lower()) for role in claims.get("realm_access", dict()).get("roles", [])
            )

        @staticmethod
        def validated():
            session["token_validate"] = time.time() + current_app.config["CFMM_TOKEN_VALIDATE_INTERVAL"]

        @classmethod
        def find(cls, force=True, **kwargs):
            try:
                kwargs["session"] = session["_id"]
            except (KeyError, AttributeError, TypeError):
                pass
            item = cls.query.filter_by(**kwargs).one_or_none()
            if not item and force:
                item = cls(**kwargs)
                db.session.add(item)
            return item

        def save(self):
            db.session.add(self)
            db.session.commit()

        @property
        def roles(self):
            if getattr(self, "_roles", None) is None:
                self.populate()
            return self._roles

    return OAuth2Token
