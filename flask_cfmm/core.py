from typing import Type

import click
from flask import Flask, request, has_request_context, url_for
from flask_admin import Admin as BaseAdmin, helpers as admin_helpers
from flask_babel import Babel, gettext
from flask_migrate import Migrate
from flask_moment import Moment

import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    from flask_security import current_user

from flask_sqlalchemy import SQLAlchemy
from flask_sqlalchemy.model import Model
from flask_wtf import CSRFProtect
from sqlalchemy import event
from sqlalchemy.dialects.sqlite.base import SQLiteDialect
from urllib.parse import urlsplit, urlunsplit

from .collect import Collect
from .configure import configure_defaults
from .database import create_database
from .datastore import CFMMUserDatastore, DisableTokenRenewal
from .security import CFMMSecurity
from .views import create_blueprint
from .translations import bp as translation
from .resource import create_client, ResourceProtection, update_registration_token, RegistrationTokenConfiguration
from .cli import log_change
from .configuration import ConfigurationStorage
from .exceptions import RedirectException


class Admin(BaseAdmin):
    def __init__(self, app, **kwargs):
        # Patch flask_admin use of deprecated WTForms tuple flags
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            from flask_admin.contrib.sqla.validators import Unique
            from flask_admin.form.validators import FieldListInputRequired
        Unique.field_flags = {"unique": True}
        FieldListInputRequired.field_flags = {"required": True}
        # End patching

        kwargs.setdefault("template_mode", "bootstrap4")
        kwargs.setdefault("base_template", "cfmm/base.html")
        kwargs.setdefault("url", "/")
        kwargs.setdefault("static_url_path", app.config.get("STATIC_URL_PATH") or "/admin/static")
        kwargs.setdefault("name", app.config["TITLE"])
        super().__init__(app=app, **kwargs)

    def _init_extension(self):
        super()._init_extension()

        decorator = self.app.extensions["security"].context_processor

        @decorator
        def security_context_processor():
            return dict(
                admin_base_template=self.base_template, admin_view=self.index_view, h=admin_helpers, get_url=url_for
            )

        @self.app.errorhandler(403)
        @self.app.errorhandler(401)
        def unauthorized(error):
            return (
                self.index_view.render(
                    "cfmm/unauthorized.html", recovery_url=url_for(self.index_view.endpoint + ".index")
                ),
                error.code,
            )

        @self.app.errorhandler(404)
        def notfound(error):
            # Token renewal should not happen during 404 handling
            with DisableTokenRenewal(current_user):
                return self.index_view.render("cfmm/notfound.html"), error.code

        @self.app.errorhandler(500)
        def internal_error(error):
            self.app.logger.error(gettext("Internal error: {err}").format(err=str(error)))
            try:
                self.app.extensions["cfmm"].db.session.rollback()
            except (KeyError, AttributeError):
                pass
            return self.index_view.render("cfmm/internal.html"), error.code

        @self.app.errorhandler(RedirectException)
        def handle_redirect_exception(e):
            return e.response


# noinspection PyUnusedLocal
def _set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON;")
    cursor.close()


class CFMMFlask(object):
    def __init__(self, app: Flask = None, db: SQLAlchemy = None, user_model: Type[Model] = None, config: str = None):
        self.user_model = user_model
        self.csrf = CSRFProtect()
        self.authenticator = None
        self.protection = ResourceProtection()
        self.db = db
        self.client_factory = create_client
        self.configuration = None

        if app is not None:
            self.init_app(app, db=db, user_model=user_model, config=config)

    def init_app(
        self,
        app: Flask,
        db: SQLAlchemy = None,
        user_model: Type[Model] = None,
    ):
        app.extensions["cfmm"] = self
        configure_defaults(app)
        db = db or self.db or create_database()
        self.db = db
        db.init_app(app)

        # Import must occur after setting database
        self.configuration = ConfigurationStorage(self.db)

        with app.app_context():
            if isinstance(db.engine.dialect, SQLiteDialect):
                event.listen(db.engine, "connect", _set_sqlite_pragma)

        def get_locale():
            """
            Configure locale
            """
            if has_request_context():
                return request.accept_languages.best_match(app.config.get("LANGUAGES", dict()).keys())
            return None

        self.csrf.init_app(app)
        Babel(app, locale_selector=get_locale)
        Migrate(app, db)

        app.config.setdefault("COLLECT_STORAGE", "flask_cfmm.collect")
        Collect(app)

        # Register a Blueprint for access to the templates
        # This must be before Security so the Blueprint is searched first
        create_blueprint(app)

        # Setup Flask-Security
        self.user_model = user_model or self.user_model

        # noinspection PyUnresolvedReferences
        CFMMSecurity(
            app,
            CFMMUserDatastore(db, self.user_model, self.user_model.role_model),
            register_blueprint=False,
        )

        app.register_blueprint(translation)

        moment = Moment()
        moment.init_app(app)

        @app.cli.command("oidc")
        @click.option("-t", "--token", required=True, help="Initial Access Token used to create client")
        @click.option("-s", "--server", required=True, multiple=True, help="URL used to access server")
        @click.option("-r", "--redirect_url", multiple=True, help="Additional supported redirect URLs")
        @click.option("--theme", help="Keycloak theme to use")
        @click.option("--access_token", help="Fallback access token to re-generate registration token")
        def register_resources(token, server, redirect_url, theme, access_token):
            for value in server + redirect_url:
                try:
                    url_parts = urlsplit(value)
                    if url_parts.scheme not in ("http", "https"):
                        raise Exception
                    urlunsplit(url_parts)
                except Exception:
                    raise click.ClickException(f"Invalid server {value}")

            log_change(
                self.client_factory(
                    app, token, server, redirect_urls=redirect_url, theme=theme, access_token=access_token
                )
            )

        @app.cli.command("registration")
        @click.option(
            "-t", "--token", required=True, help="Access token to generate registration access token for client"
        )
        def client_registration(token):
            registration = RegistrationTokenConfiguration(app, "registration_token")
            log_change(update_registration_token(app, token, registration))
