from flask_admin.contrib.sqla.filters import FilterConverter, filters
from flask_admin.model.filters import BaseFilter
from flask_admin.model.helpers import prettify_name
from flask_babel import gettext
from sqlalchemy import or_, and_


class Options(object):
    def __init__(self, column):
        self.column = column

    def __iter__(self):
        self.data = self.column.property.mapper.class_.query.all()
        self.iter_ = self.data.__iter__()
        return self

    def __next__(self):
        return self.next()

    def next(self):
        model = next(self.iter_)
        keys = list()
        for column in self.column.property.target.primary_key:
            keys.append(getattr(model, column.key))
        return tuple(keys), str(model)


class FilterRelationship(BaseFilter):
    def operation(self):
        raise NotImplementedError

    def apply(self, query, value):
        raise NotImplementedError

    def column_condition(self, column, value):
        raise NotImplementedError

    def clean(self, value):
        # Strip out the surrounding ()
        value.strip("()")
        return [v.strip() for v in value.split(",") if v.strip()]

    def condition(self, value):
        return and_(*[self.column_condition(c, v) for c, v in zip(self.column.property.target.primary_key, value)])

    @property
    def many(self):
        return self.column.property.secondaryjoin is not None

    def __init__(self, column, data_type=None):
        self.column = column
        super().__init__(prettify_name(self.column.key), Options(self.column), data_type)


class FilterMember(FilterRelationship):
    def operation(self):
        if self.many:
            return gettext("contains")
        else:
            return gettext("is")

    def column_condition(self, column, value):
        return column == value

    def apply(self, query, value, alias=None):
        cond = self.condition(value)
        if self.many:
            return query.filter(self.column.any(cond))
        else:
            return query.join(self.column).filter(cond)


class FilterNotMember(FilterRelationship):
    def operation(self):
        if self.many:
            return gettext("not contains")
        else:
            return gettext("is not")

    def column_condition(self, column, value):
        return column != value

    def apply(self, query, value, alias=None):
        cond = self.condition(value)
        if self.many:
            return query.filter(or_(self.column.any(cond), ~self.column.any()))
        else:
            return query.join(self.column).filter(cond)


def member_filters(column):
    return [FilterMember(column), FilterNotMember(column)]


class CFMMFilterConverter(FilterConverter):
    def convert(self, type_name, column, name, **kwargs):
        if type_name == "Variant":
            # noinspection PyProtectedMember
            type_name = column.type._type_affinity.__name__

        return super().convert(type_name, column, name, **kwargs)

    @filters.convert("IPNetworkType")
    def conv_ipnetwork(self, column, name, **kwargs):
        return [f(column, name, **kwargs) for f in self.strings]
