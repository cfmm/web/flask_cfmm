import typing
from sqlalchemy.orm import Mapped

if typing.TYPE_CHECKING:
    from flask_sqlalchemy import SQLAlchemy


class ConfigurationStorage:
    def __init__(self, db: "SQLAlchemy"):
        self.db = db
        for mapper in db.Model.registry.mappers:
            name = str(mapper.class_)
            if name.startswith("<class 'flask_cfmm.configuration.") and name.endswith(".Configuration'>"):
                self.record_cls = mapper.class_
                break
        else:

            class Configuration(db.Model):
                __tablename__ = "configuration"

                id: Mapped[int] = db.mapped_column("id", db.Integer(), primary_key=True, nullable=False)
                key: Mapped[str] = db.mapped_column("key", db.String(length=1024), nullable=False, unique=True)
                value: Mapped[str] = db.mapped_column("value", db.String(length=2048), nullable=False)

                def __str__(self):
                    return self.value

                __table_args__ = (
                    {
                        "mysql_engine": "InnoDB",
                        "mysql_charset": "utf8",
                        "mysql_collate": "utf8_unicode_ci",
                    },
                )

            self.record_cls = Configuration

    def get(self, key: str) -> str:
        record = self.record_cls.query.filter(self.record_cls.key == key).one_or_none()
        return record.value if record else None

    def set(self, key: str, value: str):
        record = self.record_cls.query.filter(self.record_cls.key == key).one_or_none()
        if record is None:
            record = self.record_cls(key=key, value=value)
            self.db.session.add(record)
        else:
            record.value = value

        self.db.session.commit()

    def remove(self, key: str):
        record = self.record_cls.query.filter(self.record_cls.key == key).one_or_none()
        if record:
            self.db.session.delete(record)
            self.db.session.commit()
