from pathlib import Path

import click
from babel.messages.frontend import compile_catalog, extract_messages, init_catalog, update_catalog
from flask import Blueprint, current_app

from .cli import log_change

bp = Blueprint("translate", __name__)


@bp.cli.command("compile")
def compile_translations():
    """
    Compiles translation code
    """
    directory = Path(current_app.root_path).joinpath("translations")
    # Check for expired translations
    do_compile = False
    for filename in directory.glob("**/*.po"):
        if filename.is_file():
            target = filename.with_suffix(".mo")

            if not target.is_file() or (filename.stat().st_mtime > target.stat().st_mtime):
                do_compile = True
                break

    if do_compile:
        cmd = compile_catalog()
        cmd.domain = ["messages"]
        cmd.directory = directory
        cmd.run()

    log_change(do_compile)


@bp.cli.command("extract")
def extract():
    cmd = extract_messages()
    cmd.mapping_file = "babel.cfg"
    cmd.output_file = "messages.pot"
    cmd.keywords = ["lazy_gettext"]
    cmd.input_paths = Path(current_app.root_path).parent.__str__()
    cmd.run()


@bp.cli.command("init")
@click.argument("locale")
def init(locale):
    cmd = init_catalog()
    cmd.locale = locale
    cmd.input_file = "messages.pot"
    cmd.run()


@bp.cli.command("update")
def update():
    directory = Path(current_app.root_path).joinpath("translations")
    cmd = update_catalog()
    cmd.input_file = "messages.pot"
    cmd.output_dir = directory
    cmd.run()
