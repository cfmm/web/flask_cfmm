import typing
import json
import requests
from functools import cached_property
from urllib.parse import urlsplit, urlunsplit
from contextlib import contextmanager
from deepdiff import DeepDiff

from authlib.integrations.flask_oauth2.resource_protector import (
    ResourceProtector as _ResourceProtector,
    MissingAuthorizationError,
)
from authlib.oauth2.rfc6750 import (
    BearerTokenValidator as _BearerTokenValidator,
    InvalidTokenError,
    InsufficientScopeError,
)
from authlib.oauth2.rfc7662 import IntrospectTokenValidator as _IntrospectTokenValidator

from flask import current_app, url_for, Flask, abort, make_response
from keycloak.exceptions import KeycloakError
from flask_security.proxies import _security


from .datastore import CFMMUserDatastore, TokenInvalid
from .keycloak import (
    KeycloakUMA,
    KeycloakOpenID,
    KeycloakOpenIDConnection,
    KeycloakAuthenticationError,
    KeycloakPostError,
)
from .configuration import ConfigurationStorage


def get_keycloak(app):
    url = app.config.get("KEYCLOAK_META_URL")
    parts = urlsplit(url)

    # Extract the realm from the keycloak meta_url
    realm = parts.path.split("/realms/")[1].split("/")[0]
    client_id = app.config.get("KEYCLOAK_CLIENT_ID")
    client_secret = app.config.get("KEYCLOAK_CLIENT_SECRET")

    url = urlunsplit((parts.scheme, parts.netloc, "", "", ""))

    return dict(server_url=url, realm_name=realm, client_id=client_id, client_secret_key=client_secret, verify=True)


class BearerTokenValidator(_BearerTokenValidator):
    def authenticate_token(self, token_string):
        datastore: CFMMUserDatastore = current_app.extensions["security"].datastore
        token = datastore.token_cls
        return token.query.filter(token.access_token == token_string).first()


class IntrospectTokenValidator(_IntrospectTokenValidator):
    def introspect_token(self, token_string):
        datastore: CFMMUserDatastore = current_app.extensions["security"].datastore
        try:
            token = datastore.client.introspect(token_string)

            # Introspection could just parse the token, which will *not* set active
            token.setdefault("active", True)
        except TokenInvalid:
            token = dict(active=False)
        return token


class ResourceProtector(_ResourceProtector):
    resource_type = "flask_cfmm:protector"

    def __init__(self, name=None):
        super().__init__()
        self.register_token_validator(IntrospectTokenValidator())
        self.permissions = dict()

        if name is not None:
            self.resource_type = name

    @cached_property
    def client(self):
        return KeycloakUMA(KeycloakOpenIDConnection(**get_keycloak(current_app)))

    def validate_request(self, scopes, request, **kwargs):
        data = scopes
        scopes = data.get("scopes", None)
        permissions = data.get("permissions", None)

        if permissions is not None:
            try:
                _, token_string = self.parse_request_authorization(request)
            except MissingAuthorizationError:
                try:
                    try:
                        ticket: dict = self.client.permission_ticket_create(permissions)
                    except KeycloakPostError as err:
                        if b"invalid_resource_id" in err.response_body:
                            # Try to look up the resource_ids again
                            # This typically might happen during testing where the client is recreated
                            for permission in permissions:
                                setattr(permission, "resource_id", None)
                            ticket: dict = self.client.permission_ticket_create(permissions)
                        else:
                            raise
                    realm = self.client.connection.realm_name
                    url = self.client.connection.server_url
                    rsp = make_response("", 401)
                    rsp.headers.add_header(
                        "WWW-Authenticate",
                        f'UMA realm="{realm}",as_uri="{url}/realms/{realm}",ticket="{ticket["ticket"]}"',
                    )
                except (KeycloakAuthenticationError, KeycloakPostError):
                    rsp = make_response("UMA Authorization Server Unreachable", 403)
                    rsp.headers.add_header("Warning", '199 - "UMA Authorization Server Unreachable"')

                abort(rsp)

            try:
                if not self.client.permissions_check(token_string, permissions):
                    raise InsufficientScopeError
            except KeycloakError:
                raise InvalidTokenError

        token = super().validate_request(scopes, request, **kwargs)

        # Set the current_user based on userinfo token
        datastore: CFMMUserDatastore = _security.datastore
        user = datastore.get_user(userinfo=token)
        if user is not None:
            try:
                _, token_string = self.parse_request_authorization(request)
                user.token = datastore.token_cls(access_token=token_string, renewable=False)
            except Exception:
                pass
            _security.login_manager._update_request_context_with_user(user)

        return token

    def __call__(self, scopes=None, optional=False, criteria: typing.Union[None, typing.Dict] = None):
        data = dict()
        if criteria is not None:
            data.update(criteria)
            for permission in criteria.get("permissions", []):
                if not hasattr(permission, "resource_id"):
                    setattr(permission, "resource_id", None)
                self.register_permission(permission.resource, permission.scope)

        data.setdefault("scopes", scopes)
        return super().__call__(scopes=data, optional=optional)

    def register_permission(self, resource, scope):
        self.permissions.setdefault(resource, set())
        if scope:
            self.permissions[resource].add(scope)

    def create_resources(self):
        changed = False
        if not self.permissions:
            return changed

        # noinspection PyShadowingNames
        def make_payload(resource, scopes):
            # Ideally the "_id" would be set by the resource server. However, keycloak cannot handle custom "_id" values
            # during the issue of an RPT. The custom _id can be used in obtaining the permission ticket (an invalid _id
            # generates an error), but when the RPT is requested keycloak claims the resource does not exist. If you
            # allow keycloak to set its own UUID base _id then the permission ticket and RPT requests are both
            # successful.
            return {
                "name": resource,
                "type": self.resource_type,
                "displayName": resource,
                "resource_scopes": [{"name": scope} for scope in scopes],
                "ownerManagedAccess": True,
            }

        client = self.client
        resources = dict(self.permissions)

        def values_differ(a, b):
            result = DeepDiff(
                a,
                b,
                ignore_order=True,
                report_repetition=True,
            )
            return result

        # update existing resources
        for resource in client.resource_set_list():
            if resource["type"] == self.resource_type:
                name = resource["name"]
                resource_id = resource["_id"]

                scopes = resources.pop(name, None)
                if scopes is not None:
                    payload = make_payload(name, scopes)
                    for key in payload:
                        if (key not in resource) or values_differ(payload[key], resource[key]):
                            client.resource_set_update(resource_id, payload)
                            changed = True
                            break
                else:
                    # Delete resources that have been removed
                    client.resource_set_delete(resource_id)
                    changed = True

        # Create non-existing resources
        for name, scopes in resources.items():
            client.resource_set_create(make_payload(name, scopes))
            changed = True

        return changed


class ResourceProtection:
    def __init__(self):
        self._registry = dict()

    def register(self, protector: ResourceProtector):
        self._registry[protector.resource_type] = protector

    def remove(self, protector: ResourceProtector):
        self._registry.pop(protector.resource_type, None)

    @property
    def protectors(self):
        return self._registry.values()

    def get(self, name=None) -> ResourceProtector:
        lookup = name or ResourceProtector.resource_type
        protector = self._registry.get(lookup, ResourceProtector(name=lookup))
        self._registry[lookup] = protector
        return protector

    def generate_resources(self):
        changed = False
        for protector in self.protectors:
            changed |= protector.create_resources()
        return changed


def create_or_update_access(client, policy, resource_name):
    changed = False
    try:
        scope = policy["scopes"][0]
        resource_ids = client.resource_set_list_ids(name=resource_name, exact_name=True, scope=scope, maximum=1)
    except Exception:
        scope = None
        resource_ids = None
        pass
    if resource_ids:
        try:
            client.policy_resource_create(resource_id=resource_ids[0], payload=policy)
            changed = True
        except KeycloakPostError as err:
            if err.response_code == 409:
                existing_policy = client.policy_query(
                    resource=resource_name,
                    scope=scope,
                    name=policy["name"],
                    maximum=1,
                )[0]
                for key in ("name", "description", "scopes", "clients", "roles"):
                    if key not in policy and key not in existing_policy:
                        continue
                    if key not in policy or key not in existing_policy or policy[key] != existing_policy[key]:
                        client.policy_update(policy_id=existing_policy["id"], payload=policy)
                        changed = True
                        break
            else:
                raise

    return changed


class RegistrationToken:
    def get(self):
        raise NotImplementedError()

    def set(self, value):
        raise NotImplementedError()

    def remove(self, value):
        raise NotImplementedError()


class RegistrationTokenConfiguration(RegistrationToken):
    def __init__(self, app, name):
        @contextmanager
        def configurator() -> ConfigurationStorage:
            with app.app_context():
                yield app.extensions["cfmm"].configuration

        def set(value):
            with configurator() as storage:
                storage.set(key=name, value=value)

        def get():
            with configurator() as storage:
                return storage.get(key=name)

        def remove():
            with configurator() as storage:
                storage.remove(key=name)

        self.get = get
        self.set = set
        self.remove = remove


def create_or_update(app, token, payload, registration: RegistrationToken, changed=False, access_token=None):
    kwargs = get_keycloak(app)
    if "clientId" in payload:
        kwargs["client_id"] = payload["clientId"]
    kwargs.pop("client_secret", None)

    registration_token = registration.get()

    if registration_token is None:
        changed = True
        client = KeycloakOpenID(**kwargs)
        try:
            response = client.register_client(token, payload)
            registration.set(response["registrationAccessToken"])
        except KeycloakPostError as err:
            # Generate a missing registration token error when client already exists
            data = json.loads(err.response_body)
            if (
                data["error"] == "invalid_client_metadata"
                or data.get("error_description", "") == "Client Identifier in use"
            ):
                raise Exception("Missing registration_token configuration")
            else:
                raise
    else:
        client = KeycloakOpenID(**kwargs)

        retry = 0
        while retry < 2:
            try:
                response = client.get_client(registration_token, client_id=client.client_id)
                break
            except KeycloakAuthenticationError:
                if retry == 0 and update_registration_token(app, access_token, registration):
                    registration_token = registration.get()
                    retry = 1
                else:
                    retry = 2
        else:
            registration.remove()
            raise Exception("Invalid registration token for get")

        registration_token = response.pop("registrationAccessToken")

        #
        # 'Special' client scope 'service_account' "automatically assigned to and unassigned from the client when the
        # serviceAccountsEnabled option is set or unset in the client configuration"
        # See https://www.keycloak.org/docs/latest/upgrading/index.html#migrating-to-25-0-0
        # Updating an existing client via API with 'service_account' missing from 'defaultClientScopes' effectively
        # unsets it.
        # Append 'service_account' to defaultClientScopes for updates only
        # Cannot add 'service_account' to 'defaultClientScopes' when client is created, because this results in
        # Keycloak error 'error="not_allowed", client_registration_policy="Allowed Client Scopes"'
        # Make backwards compatible by also checking whether 'service_account' in 'defaultClientScopes' of response to
        # the above create or get request.
        try:
            if (
                payload.get("serviceAccountsEnabled", False)
                and "service_account" in response["defaultClientScopes"]
                and "service_account" not in payload["defaultClientScopes"]
            ):
                payload["defaultClientScopes"] += ["service_account"]
        except KeyError:
            pass

        retry = 0
        while retry < 2:
            try:
                response2 = client.update_client(registration_token, client_id=client.client_id, payload=payload)
                break
            except KeycloakAuthenticationError:
                if retry == 0 and update_registration_token(app, access_token, registration):
                    registration_token = registration.get()
                    retry = 1
                else:
                    retry = 2
        else:
            registration.remove()
            raise Exception("Invalid registration token for update")

        registration.set(response2["registrationAccessToken"])

        if not changed:

            def ordered(obj):
                if isinstance(obj, dict):
                    return sorted((k, ordered(v)) for k, v in obj.items())
                if isinstance(obj, list):
                    return sorted(ordered(x) for x in obj)
                else:
                    return obj

            for key in response.keys():
                try:
                    if ordered(response[key]) != ordered(response2[key]):
                        changed = True
                        break
                except KeyError:
                    changed = True
                    break

    return changed


def create_client(app: Flask, token, servers, redirect_urls=None, theme=None, access_token=None):
    kwargs = get_keycloak(app)
    changed = False

    client_id = kwargs["client_id"]
    client_secret = kwargs["client_secret_key"]

    if not client_secret:
        raise ValueError("OIDC client secret cannot be empty")

    with app.test_request_context():
        suffixes = (
            url_for("security.authorize"),
            url_for("security.landing"),
            url_for("security.deauthorize"),
        )
        backchannel = url_for("security.logout_oidc_session")

    redirect_urls = set(redirect_urls or [])
    web_origins = set(servers)
    web_origins.update("{uri.scheme}://{uri.netloc}/".format(uri=urlsplit(url)) for url in redirect_urls)
    redirect_urls.update(f"{x}{y}*" for x in servers for y in suffixes)

    payload = {
        "clientId": client_id,
        "name": client_id,
        "adminUrl": servers[0],
        "surrogateAuthRequired": False,
        "enabled": True,
        "alwaysDisplayInConsole": False,
        "clientAuthenticatorType": "client-secret",
        "secret": client_secret,
        "redirectUris": list(redirect_urls),
        "webOrigins": list(web_origins),
        "description": "flask_cfmm registered client",
        "notBefore": 0,
        "bearerOnly": False,
        "consentRequired": False,
        "standardFlowEnabled": True,
        "implicitFlowEnabled": False,
        "directAccessGrantsEnabled": False,
        "serviceAccountsEnabled": True,
        "authorizationServicesEnabled": True,
        "publicClient": False,
        "frontchannelLogout": False,
        "protocol": "openid-connect",
        "attributes": {
            "login_theme": theme or "keycloak",
            "backchannel.logout.url": f"{servers[0]}{backchannel}",
            "backchannel.logout.session.required": "true",
            "backchannel.logout.revoke.offline.tokens": "true",
        },
        "defaultClientScopes": ["profile", "roles", "email", "web-origins"],
        "optionalClientScopes": ["address", "microprofile-jwt", "offline_access"],
        "authenticationFlowBindingOverrides": {},
        "fullScopeAllowed": True,
        "nodeReRegistrationTimeout": -1,
        "authorizationSettings": {
            "allowRemoteResourceManagement": True,
            "policyEnforcementMode": "ENFORCING",
            "resources": [],
            "policies": [],
            "scopes": [],
            "decisionStrategy": "AFFIRMATIVE",
        },
    }

    registration = RegistrationTokenConfiguration(app, "registration_token")
    changed |= create_or_update(app, token, payload, registration, access_token=access_token)

    # Always update the resources because (a) the may have changed and (b) keycloak refuses to set them during creation
    with app.app_context():
        changed |= app.extensions["cfmm"].protection.generate_resources()

    return changed


def update_registration_token(app, token, registration: RegistrationToken):
    if token is None:
        return False

    kwargs = get_keycloak(app)
    client_id = kwargs["client_id"]
    realm = kwargs["realm_name"]
    root = kwargs["server_url"]

    headers = {"Authorization": f"Bearer {token}", "Content-Type": "application/json"}

    # Get the client information
    url = f"{root}/admin/realms/{realm}/clients/?clientId={client_id}"
    rsp = requests.get(url, headers=headers)
    if rsp.status_code != 200:
        return False

    # Generate a new registration access token
    try:
        data = rsp.json()
        url = f"{root}/admin/realms/{realm}/clients/{data[0]['id']}/registration-access-token"
    except (KeyError, IndexError):
        return False

    rsp = requests.post(url, headers=headers)

    if rsp.status_code != 200:
        return False

    # Save the registration access token
    registration.set(rsp.json()["registrationAccessToken"])

    return True
