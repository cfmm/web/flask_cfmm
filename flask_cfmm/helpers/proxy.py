from werkzeug.middleware.proxy_fix import ProxyFix as ProxyFixBase
import typing as t

if t.TYPE_CHECKING:
    from _typeshed.wsgi import StartResponse, WSGIEnvironment


class ProxyFix(ProxyFixBase):
    def __init__(self, *args, proxy: t.Optional[str] = None, **kwargs):
        self.proxy = proxy
        # if no proxy is registered, assume all traffic comes from proxy
        self.from_proxy = self.proxy is None
        super().__init__(*args, **kwargs)

    def _get_real_value(self, trusted: int, value: t.Optional[str]) -> t.Optional[str]:
        if not self.from_proxy:
            return None
        return super()._get_real_value(trusted, value)

    def __call__(self, environ: "WSGIEnvironment", start_response: "StartResponse") -> t.Iterable[bytes]:
        # Check that the request actually comes from the registered proxy
        # If no proxy was registered, assume all traffic comes from proxy
        self.from_proxy = self.proxy == environ.get("REMOTE_ADDR") if self.proxy is not None else True
        return super().__call__(environ, start_response)
