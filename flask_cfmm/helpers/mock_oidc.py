import random
import string
from itertools import cycle
from time import time
from uuid import uuid4

from authlib.jose.errors import InvalidClaimError
from flask import redirect

from ..security import create_user
from ..configure import get_role


def oidc_mocker(mocker):
    sid = str(uuid4())
    data = {
        "preferred_username": "admin",
        "email": "admin@example.local",
        "sub": "admin",
        "email_verified": True,
        "name": "Admin User",
        "given_name": "Admin",
        "family_name": "User",
        "aud": "development",
        "azp": "development",
        "sid": sid,
        "session_state": sid,
    }

    metadata = {
        "end_session_endpoint": "https://some.other.host",
    }

    def userinfo():
        return data

    def load_server_metadata():
        return metadata

    def authorize(redirect_uri):
        return redirect(redirect_uri)

    def authorize_access_token():
        timestamp = int(time())
        expire = timestamp + 300
        data["exp"] = expire
        data["iat"] = expire
        data["auth_time"] = timestamp

        token = {
            "token_type": "Bearer",
            "expires_in": 300,
            "refresh_expires_in": 1800,
            "expires_at": timestamp + 300,
            "not-before-policy": timestamp - 1000000,
            "scope": "openid profile email",
            "userinfo": data,
            "access_token": str(uuid4()),
            "refresh_token": str(uuid4()),
            "id_token": str(uuid4()),
            "session_state": data["sid"],
            "sid": data["sid"],
        }

        return token

    mocker.patch("authlib.integrations.flask_client.apps.FlaskOAuth2App.authorize_redirect", side_effect=authorize)
    mocker.patch(
        "authlib.integrations.flask_client.apps.FlaskOAuth2App.authorize_access_token",
        side_effect=cycle([InvalidClaimError(claim="iss"), mocker.DEFAULT]),
        wraps=authorize_access_token,
    )
    mocker.patch("authlib.integrations.flask_client.apps.FlaskOAuth2App.userinfo", side_effect=userinfo)
    mocker.patch(
        "authlib.integrations.flask_client.apps.FlaskOAuth2App.load_server_metadata", side_effect=load_server_metadata
    )

    return data


def login(client, role=None, username=None):
    with client.application.app_context():
        role = role or get_role("admin_role")

        datastore = client.application.extensions["security"].datastore

        username = username or "".join(random.choice(string.ascii_lowercase) for _ in range(10))
        user = datastore.find_user(username=username) or create_user(username)
        if not user.has_role(role):
            datastore.add_role_to_user(user, datastore.find_or_create_role(role))

        with client.application.test_request_context():
            with client.session_transaction() as session:
                user.token = datastore.token_cls.find(name=datastore.client.name, user=user)
                session["_user_id"] = user.fs_uniquifier

        datastore.commit()

        user_id = user.id
    return user_id
