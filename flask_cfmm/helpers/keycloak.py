import time
from .testing import wait_until_responsive, is_responsive
from keycloak.keycloak_admin import KeycloakAdmin, KeycloakDeleteError
from keycloak.exceptions import KeycloakAuthenticationError


def create_service(url, users=None, realm="TEST", username="jboss_admin", password="admin", management_url=None):
    def is_ready():
        ready = is_responsive(url + "/health/ready")
        if management_url is not None:
            ready |= is_responsive(management_url + "/health/ready")
        return ready

    wait_until_responsive(timeout=120.0, pause=1, check=is_ready)
    # Even though keycloak is ready, the master user may not be setup
    for delay in range(1, 5):
        try:
            admin = KeycloakAdmin(url, username=username, password=password, user_realm_name="master")
            break
        except KeycloakAuthenticationError:
            time.sleep(delay)
    else:
        raise KeycloakAuthenticationError("Unable to login to keycloak")

    # Configure keycloak
    try:
        admin.delete_realm(realm)
    except KeycloakDeleteError:
        pass

    admin.create_realm(payload={"realm": realm, "enabled": True}, skip_exists=False)
    admin.connection.realm_name = realm

    if users is not None:
        for key, value in users.items():
            payload = {
                "username": key,
                "credentials": [{"value": value, "type": "password"}],
                "enabled": True,
                "emailVerified": True,
                "email": f"{key}@localhost",
                "requiredActions": [],
                "firstName": "User",
                "lastName": key,
            }
            admin.create_user(payload)

    return admin


def create_client(app, service, create_user, count=None, expiration=None):
    token = service.create_initial_access_token(count=count or 1, expiration=expiration or 10)

    with app.app_context():
        db = app.extensions["sqlalchemy"]

        # noinspection PyUnresolvedReferences
        db.session.remove()
        db.session.close()
        db.drop_all()
        db.create_all()
        create_user("admin")

    runner = app.test_cli_runner()
    result = runner.invoke(args=["oidc", "--token", token["token"], "--server", "http://localhost"])

    assert result.exit_code == 0

    client_name = app.config["KEYCLOAK_CLIENT_ID"]
    client_id = service.get_client_id(client_name)
    client = service.get_client(client_id)

    yield client

    service.delete_client(client_id)
