from __future__ import annotations

from flask import session, request, Blueprint, Flask
from flask_babel import lazy_gettext

# noinspection PyProtectedMember
from flask_wtf.csrf import CSRFError


def create_blueprint(app: Flask) -> None:
    bp = Blueprint(
        "cfmm",
        __name__,
        static_url_path=app.config.get("STATIC_URL_PATH") or "/cfmm/static",
        template_folder="templates",
        static_folder="static",
    )

    # Register before_request to update the session for timeout
    @bp.before_app_request
    def session_timeout():
        session.modified = True

    # Register
    @bp.errorhandler(CSRFError)
    def handler(exception: Exception) -> tuple[str, int]:
        app.logger.error(
            lazy_gettext("CSRFError {error} {referrer} {host}").format(
                error=str(exception), referrer=request.referrer, host=request.host
            )
        )
        return str(exception), 400

    app.register_blueprint(bp)
