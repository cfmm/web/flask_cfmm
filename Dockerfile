FROM python:3.12-alpine AS compiler

ARG TARGETPLATFORM
RUN --mount=type=cache,target=/var/cache/apk if [ "$TARGETPLATFORM" = "linux/arm64" ]; then apk add --quiet libffi-dev; fi
RUN --mount=type=cache,target=/var/cache/apk apk add --quiet gcc libc-dev mariadb-dev

RUN python -m venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH" \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1

COPY pyproject.toml ./
RUN --mount=type=cache,target=/root/.cache/pip pip install toml && python -c $'import toml\na=toml.load("pyproject.toml")["project"]\nfor x in a["dependencies"]+a["optional-dependencies"]["dev"]: print(x)' | pip install -r /dev/stdin

FROM python:3.12-alpine AS runner

RUN --mount=type=cache,target=/var/cache/apk apk add --quiet mariadb-connector-c

COPY --from=compiler /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH" \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1
