Flask extension for configuring basic Keycloak user authentication and providing
commonly used objects. The Flask app is configured with security and CSRF protection.
