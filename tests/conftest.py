from collections import OrderedDict

# noinspection PyPackageRequirements
import pytest
import sqlalchemy as sa
from flask import Flask, make_response, redirect

from flask_cfmm import CFMMFlask, Admin, UserView, RoleView, AuthorizationView, AuthorizationAdminView
from flask_cfmm.configure import configure_defaults
from flask_cfmm.database import create_database
from flask_cfmm.exceptions import (
    BadRequest,
    NotAuthorized,
    NotAuthenticated,
    InvalidUrl,
    RequestNotFound,
    NotAcceptable,
    RedirectException,
)
from flask_cfmm.models import RoleMixin, UserMixin, AuthorizationMixin, DateTracking, Permission
from flask_cfmm.security import DictionaryAuthenticator, create_user
from flask_cfmm.cli import authorization_factory
from flask_cfmm.datastore import TokenInvalid

# noinspection PyPackageRequirements
from keycloak.uma_permissions import UMAPermission

from tests.app import RouteFiltersView

from flask_cfmm.helpers.testing import wait_until_responsive
from flask_cfmm.helpers.keycloak import create_service, create_client
from flask_cfmm.resource import create_or_update_access, KeycloakUMA, KeycloakOpenIDConnection


def wait_for_database(uri) -> sa.engine.Engine:
    def check_database():
        # noinspection PyBroadException
        try:
            engine = sa.create_engine(uri)
            engine.connect()
            return True
        except Exception:
            return False

    wait_until_responsive(timeout=30.0, pause=1, check=check_database)


@pytest.fixture(scope="session")
def mysql_database():
    database = "mysql://test_user:secret@mysql:3306/test_db"
    wait_for_database(database)
    yield database


@pytest.fixture(scope="session")
def postgresql_database():
    database = "postgresql+psycopg2://test_user:secret@postgres:5432/test_db"
    wait_for_database(database)
    yield database


@pytest.fixture(scope="session")
def users():
    return OrderedDict(
        {
            "admin": "secret",
            "user": "secret",
        }
    )


@pytest.fixture(scope="function")
def admin_user(users):
    return "admin", users["admin"]


@pytest.fixture(scope="function")
def general_user(users):
    return "user", users["user"]


@pytest.fixture(scope="session")
def auth_errors():
    return {
        "user": (NotAuthenticated, 401),
        "auth": (NotAuthorized, 403),
        "bad": (BadRequest, 400),
        "invalid": (InvalidUrl, 404),
        "request": (RequestNotFound, 404),
        "acceptable": (NotAcceptable, 406),
        "internal": (Exception, 400),
    }


@pytest.fixture(scope="session")
def keycloak_service(users):
    """Ensure that HTTP service is up and responsive."""
    try:
        # noinspection HttpUrlsUsage
        admin = create_service("http://keycloak:8080", management_url="http://keycloak:9000", users=users)
    except Exception as e:
        pytest.exit(str(e))
        return
    yield admin


@pytest.fixture(scope="function")
def keycloak_client(oidc_app, keycloak_service, keycloak_permission):
    yield from create_client(oidc_app, keycloak_service, create_user)


@pytest.fixture(scope="function")
def keycloak_policy(keycloak_service, keycloak_client, keycloak_permission):
    connection = KeycloakOpenIDConnection(
        server_url=keycloak_service.connection.server_url,
        realm_name=keycloak_service.connection.realm_name,
        client_id=keycloak_client["clientId"],
        client_secret_key=keycloak_client["secret"],
    )

    # Create policy
    client = KeycloakUMA(connection)
    policy = {
        "name": "Test policy",
        "description": "Policy for testing resource access",
        "scopes": [keycloak_permission.scope],
        "clients": [client.connection.client_id],
    }
    resource = keycloak_permission.resource

    # Check that a change is made when policy is created
    assert create_or_update_access(client, policy, resource) is True

    # Check that a change is not made when policy is not changed
    assert create_or_update_access(client, policy, resource) is False

    policy_id = client.policy_query(name=policy["name"], resource=resource, maximum=1)

    yield policy_id

    # Delete policy
    client.policy_delete(policy_id[0]["id"])


@pytest.fixture(scope="session")
def keycloak_permission():
    yield UMAPermission(resource="test", scope="read")


def create_app(db, mapping=None, users=None, auth_errors=None):
    app = Flask(__name__)

    class Role(RoleMixin, db.Model):
        pass

    class User(UserMixin, db.Model):
        role_model = Role

    class Authorization(AuthorizationMixin, db.Model):
        user_model = User
        upload = Permission("upload")
        test = Permission("test")

        __table_args__ = (db.Index(None, "upload"),)

    # noinspection PyUnusedLocal
    class Group(DateTracking, db.Model):
        __tablename__ = "group"
        id = db.Column("id", db.Integer(), primary_key=True, autoincrement=True)

    class RouteFilters(db.Model):
        __tablename__ = "route_filters"
        id = db.Column(
            "id",
            db.Integer,
            primary_key=True,
        )
        boolean_column = db.Column("boolean_column", db.Boolean)

    if mapping:
        app.config.from_mapping(mapping)

    app.config.setdefault("STATIC_URL_PATH", None)
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    configure_defaults(app)

    # Convenience for intercepting exception with breakpoint
    # noinspection PyUnusedLocal
    @app.errorhandler(Exception)
    def handle_exception(e):
        return e

    cfmm = CFMMFlask()
    cfmm.init_app(app, db, user_model=User)
    if users:
        cfmm.authenticator = DictionaryAuthenticator(users)

    admin = Admin(app=app, base_template="cfmm/base.html")

    admin.add_view(RouteFiltersView(RouteFilters, db.session, endpoint="filters"))
    admin.add_view(UserView(User, db.session, endpoint="user"))
    admin.add_view(RoleView(Role, db.session, endpoint="role"))
    admin.add_view(AuthorizationView(Authorization, db.session, endpoint="auth", name="Authorization"))
    admin.add_view(AuthorizationAdminView(Authorization, db.session, endpoint="auth2", name="Authorization2"))

    @app.route("/api/auth/<flag>", methods=["GET"])
    def api_auth(flag):
        # noinspection PyUnusedLocal
        @Authorization.run_task(permissions=["test"])
        def test(auth):
            if auth_errors:
                data = auth_errors.get(flag, None)
                if data:
                    raise data[0]()

            return make_response("OK", 200)

        return test()

    @app.route("/token")
    def invalid_token():
        raise TokenInvalid()

    @app.route("/redirect")
    def redirect_route():
        raise RedirectException(redirect("/redirect"))

    app.extensions["models"] = dict(user=User, role=Role, authorization=Authorization, route_filters=RouteFilters)

    authorization_factory(app, Authorization, db)
    return app


@pytest.fixture(scope="session")
def oidc_app(users, auth_errors, keycloak_service, keycloak_permission):
    return create_oidc_app(
        users,
        auth_errors,
        keycloak_service,
        keycloak_permission,
    )


@pytest.fixture(scope="session")
def postgresql_app(users, auth_errors, keycloak_service, keycloak_permission, postgresql_database):
    return create_oidc_app(
        users,
        auth_errors,
        keycloak_service,
        keycloak_permission,
        database_uri="postgresql+psycopg2://test_user:secret@postgres:5432/test_db",
    )


@pytest.fixture(scope="session")
def postgresql_psycopg3_app(users, auth_errors, keycloak_service, keycloak_permission, postgresql_database):
    return create_oidc_app(
        users,
        auth_errors,
        keycloak_service,
        keycloak_permission,
        database_uri="postgresql+psycopg://test_user:secret@postgres:5432/test_db",
    )


@pytest.fixture(scope="session")
def mysql_app(users, auth_errors, keycloak_service, keycloak_permission, mysql_database):
    return create_oidc_app(
        users,
        auth_errors,
        keycloak_service,
        keycloak_permission,
        database_uri="mysql://test_user:secret@mysql:3306/test_db",
    )


def create_oidc_app(users, auth_errors, keycloak_service, keycloak_permission, database_uri=None):
    config = dict()
    config.setdefault(
        "KEYCLOAK_META_URL",
        keycloak_service.connection.server_url
        + "/realms/"
        + keycloak_service.connection.realm_name
        + "/.well-known/openid-configuration",
    )
    config.setdefault("KEYCLOAK_CLIENT_ID", "flask_cfmm_test")
    config.setdefault("KEYCLOAK_CLIENT_SECRET", "000000000000000")
    if database_uri:
        config["SQLALCHEMY_DATABASE_URI"] = database_uri
    db = create_database()
    app = create_app(db, mapping=config, users=users, auth_errors=auth_errors)

    protector = app.extensions["cfmm"].protection.get()

    @app.route("/api/uma", methods=["GET"])
    @protector(criteria=dict(permissions=[keycloak_permission]))
    def api_uma():
        return make_response("Success", 200)

    @app.route("/api/uma2", methods=["GET"])
    @protector(criteria=None)
    def api_uma2():
        return make_response("Success", 200)

    return app


def app_fixture(app_):
    app_.app_context().push()
    db = app_.extensions["sqlalchemy"]
    with app_.test_client() as client:
        db = app_.extensions["sqlalchemy"]

        # noinspection PyUnresolvedReferences
        db.session.remove()
        db.session.close()
        db.drop_all()
        db.create_all()
        create_user("admin")
        yield client
        db.session.remove()
        db.session.close()
        db.drop_all()


@pytest.fixture()
def oidc(oidc_app):
    yield from app_fixture(oidc_app)


@pytest.fixture()
def postgresql(postgresql_app):
    yield from app_fixture(postgresql_app)


@pytest.fixture()
def postgresql_psycopg3(postgresql_psycopg3_app):
    yield from app_fixture(postgresql_psycopg3_app)


@pytest.fixture()
def mysql(mysql_app):
    yield from app_fixture(mysql_app)


@pytest.fixture()
def client(request):
    return request.getfixturevalue(request.param)
