# noinspection PyUnresolvedReferences
import pytest
from flask_cfmm.resource import (
    create_client,
    create_or_update,
    update_registration_token,
    create_or_update_access,
    KeycloakUMA,
    get_keycloak,
    RegistrationTokenConfiguration,
)
from flask_cfmm.keycloak import KeycloakOpenIDConnection, get_rpt, KeycloakOpenID


def test_protected_user(oidc, keycloak_service, keycloak_client, keycloak_policy, admin_user):
    response = oidc.get("/api/uma")
    assert response.status_code == 401

    client_id = keycloak_client["id"]
    client = keycloak_service.get_client(client_id)
    client["directAccessGrantsEnabled"] = True
    keycloak_service.update_client(client_id, client)

    client_id = keycloak_client["clientId"]
    client = KeycloakOpenID(
        server_url=keycloak_service.connection.server_url,
        realm_name=keycloak_service.connection.realm_name,
        client_id=client_id,
        client_secret_key=keycloak_client["secret"],
    )
    username, password = admin_user

    token = client.token(username=username, password=password, scope="openid", grant_type=["password"])

    token = get_rpt(response, token["access_token"])

    header = {"Authorization": "Bearer " + token["access_token"]}
    response = oidc.get("/api/uma", headers=header)
    assert response.status_code == 200


def test_protected(oidc, keycloak_service, keycloak_client, keycloak_policy, keycloak_permission):
    client_id = keycloak_client["clientId"]
    response = oidc.get("/api/uma")
    assert response.status_code == 401

    connection = KeycloakOpenIDConnection(
        server_url=keycloak_service.connection.server_url,
        realm_name=keycloak_service.connection.realm_name,
        client_id=client_id,
        client_secret_key=keycloak_client["secret"],
    )
    connection.get_token()
    token = connection.token

    header = {"Authorization": "Bearer " + token["access_token"]}
    response = oidc.get("/api/uma", headers=header)
    assert response.status_code == 200

    connection.keycloak_openid.connection.add_param_headers("Authorization", "Bearer " + token["access_token"])
    token = connection.keycloak_openid.token(
        grant_type="urn:ietf:params:oauth:grant-type:uma-ticket",
        permission=str(keycloak_permission),
        audience=client_id,
    )
    header = {"Authorization": "Bearer " + token["access_token"]}
    response = oidc.get("/api/uma", headers=header)
    assert response.status_code == 200


def test_resource_access_no_policy(oidc, oidc_app, keycloak_service, keycloak_client, keycloak_permission):
    response = oidc.get("/api/uma")
    assert response.status_code == 401

    connection = KeycloakOpenIDConnection(**get_keycloak(oidc_app))
    connection.get_token()
    header = {"Authorization": "Bearer " + connection.token["access_token"]}
    response = oidc.get("/api/uma", headers=header)
    assert response.status_code == 403


def test_resource_policy_change(oidc_app, keycloak_service, keycloak_client):
    connection = KeycloakOpenIDConnection(**get_keycloak(oidc_app))

    # Create policy
    client = KeycloakUMA(connection)
    policy = {
        "name": "Test policy",
        "description": "Policy for testing resource access",
        "scopes": ["invalid"],
        "clients": [client.connection.client_id],
    }

    assert create_or_update_access(client, policy, "invalid") is False
    assert create_or_update_access(client, {}, "invalid") is False


def test_protect_invalid_token(oidc, keycloak_service, keycloak_client, keycloak_permission):
    header = {"Authorization": "Bearer NotAValidNoken"}
    response = oidc.get("/api/uma", headers=header)
    assert response.status_code == 401


def test_renew_registration(oidc_app, keycloak_service, keycloak_client):
    token = keycloak_service.connection.token
    registration = RegistrationTokenConfiguration(oidc_app, "registration_token")
    changed = update_registration_token(oidc_app, token["access_token"], registration)
    assert changed
    token = keycloak_service.create_initial_access_token(count=1, expiration=10)
    changed = create_client(oidc_app, token["token"], ("http://localhost",))
    assert not changed, "Client changed during second registration"


def test_renew_registration_cli(oidc_app, keycloak_service, keycloak_client):
    token = keycloak_service.connection.token
    runner = oidc_app.test_cli_runner()
    result = runner.invoke(args=["registration", "--token", token["access_token"]])
    assert result.exit_code == 0
    assert result.output.endswith("<<CHANGED>>\n")


# noinspection PyUnusedLocal
def test_register_client(oidc_app, keycloak_service, keycloak_client):
    # Update a client when it already exists
    token = keycloak_service.create_initial_access_token(count=1, expiration=10)
    changed = create_client(oidc_app, token["token"], ("http://localhost",))
    assert not changed, "Client changed from initial registration"

    registration = RegistrationTokenConfiguration(oidc_app, "registration_token")
    registration.remove()

    with pytest.raises(Exception) as exc_info:
        create_client(oidc_app, token["token"], ("http://localhost",))

    assert str(exc_info.value) == "Missing registration_token configuration"

    registration.set("invalid")

    with pytest.raises(Exception) as exc_info:
        create_client(oidc_app, token["token"], ("http://localhost",))

    assert str(exc_info.value) == "Invalid registration token for get"

    assert registration.get() is None
    registration.set("invalid")

    access_token = keycloak_service.connection.token
    changed = create_client(oidc_app, token["token"], ("http://localhost",), access_token=access_token["access_token"])
    assert not changed


def test_update_client(oidc_app, keycloak_service, keycloak_client):
    token = keycloak_service.create_initial_access_token(count=1, expiration=100)

    payload = {
        "clientId": "TestClient",
        "name": "TestClient",
        "surrogateAuthRequired": False,
        "enabled": True,
        "alwaysDisplayInConsole": False,
        "clientAuthenticatorType": "client-secret",
        "description": "Fake testing client",
        "notBefore": 0,
        "bearerOnly": False,
        "consentRequired": False,
        "standardFlowEnabled": False,
        "implicitFlowEnabled": False,
        "directAccessGrantsEnabled": False,
        "serviceAccountsEnabled": True,
        "authorizationServicesEnabled": False,
        "publicClient": False,
        "frontchannelLogout": False,
        "protocol": "openid-connect",
        "authenticationFlowBindingOverrides": {},
        "fullScopeAllowed": False,
        "nodeReRegistrationTimeout": -1,
    }

    registration = RegistrationTokenConfiguration(oidc_app, "registration_token_test")
    changed = create_or_update(oidc_app, token["token"], payload, registration)
    assert changed

    client_id = keycloak_service.get_client_id(payload["clientId"])

    mapper = {
        "id": "ecce00f4-a541-4cd2-916c-bb9cbe4dbeff",
        "name": "Client ID",
        "protocol": "openid-connect",
        "protocolMapper": "oidc-usersessionmodel-note-mapper",
        "consentRequired": False,
        "config": {
            "user.session.note": "clientId",
            "id.token.claim": "true",
            "access.token.claim": "true",
            "claim.name": "clientId",
            "jsonType.label": "String",
        },
    }
    keycloak_service.add_mapper_to_client(client_id=client_id, payload=mapper)
    mapper = {
        "id": "e3eed616-323d-4314-a2e9-211ae12c3770",
        "name": "Client Host",
        "protocol": "openid-connect",
        "protocolMapper": "oidc-usersessionmodel-note-mapper",
        "consentRequired": False,
        "config": {
            "user.session.note": "clientHost",
            "id.token.claim": "true",
            "access.token.claim": "true",
            "claim.name": "clientHost",
            "jsonType.label": "String",
        },
    }
    keycloak_service.add_mapper_to_client(client_id=client_id, payload=mapper)

    changed = create_or_update(oidc_app, token["token"], payload, registration)
    assert not changed
