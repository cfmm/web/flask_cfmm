from urllib.parse import urlparse

# noinspection PyPackageRequirements
import pytest

from flask_cfmm.configure import get_role
from .test_authentication import login


def execute_view(client, view, status=200):
    def evaluate_request(endpoint, request_type):
        rsp_ = client.get(f"/{view}/{endpoint}", follow_redirects=False)
        if rsp_.status_code == 302:
            if urlparse(rsp_.location).path.strip("/") != f"{view}":
                raise ValueError("Redirect not for empty set")
        else:
            assert rsp_.status_code == status, f"{request_type} view Error. Unexpected status code {rsp_.status_code}."

    evaluate_request("", "List")
    evaluate_request("edit/?id=1", "Edit")
    evaluate_request("new/", "Create")


@pytest.mark.parametrize("client", ["oidc", "postgresql", "postgresql_psycopg3", "mysql"], indirect=True)
@pytest.mark.parametrize("view", ["user", "role", "auth2"])
def test_route(client, view, admin_user):
    login(client, *admin_user)
    execute_view(client, view)


@pytest.mark.parametrize("client", ["oidc", "postgresql", "postgresql_psycopg3", "mysql"], indirect=True)
@pytest.mark.parametrize("view", ["auth"])
def test_route_user(client, view, general_user):
    login(client, general_user[0], get_role("auth_role"))
    execute_view(client, view, 200)


@pytest.mark.parametrize("client", ["oidc", "postgresql", "postgresql_psycopg3", "mysql"], indirect=True)
@pytest.mark.parametrize("view", ["auth"])
def test_route_user_as_admin(client, view, admin_user):
    login(client, admin_user[0])
    execute_view(client, view, 403)


@pytest.mark.parametrize("client", ["oidc", "postgresql", "postgresql_psycopg3", "mysql"], indirect=True)
def test_route_filters(client, admin_user):
    login(client, admin_user[0])
    rsp_ = client.get("/filters/?flt0_boolean_column_equals=1", follow_redirects=False)
    assert rsp_.status_code == 200, "Failure to use filter on boolean column"


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_not_found(client, admin_user):
    login(client, admin_user[0])
    rsp_ = client.get("/not/found", follow_redirects=False)
    assert rsp_.status_code == 404, "Failure to render not found error"
