def test_mysql(mysql_app, keycloak_service):
    db = mysql_app.extensions["sqlalchemy"]
    with mysql_app.app_context():
        db.drop_all()
        db.create_all()


def test_postgres(postgresql_app, keycloak_service):
    db = postgresql_app.extensions["sqlalchemy"]
    with postgresql_app.app_context():
        db.drop_all()
        db.create_all()
