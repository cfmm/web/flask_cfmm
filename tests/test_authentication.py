# noinspection PyPackageRequirements
import pytest
from urllib.parse import urlparse, parse_qs
from flask_cfmm.helpers.mock_oidc import oidc_mocker, login as fake_login


def login(client, username=None, role=None):
    fake_login(client, role=role, username=username)


def logout(client):
    return client.get("logout", follow_redirects=True)


@pytest.fixture()
def mock_oidc(mocker):
    return oidc_mocker(mocker)


def test_oidc(oidc, users, mock_oidc):
    rv = oidc.post("/login", follow_redirects=True)
    assert rv.status_code == 200

    rv = oidc.get("/logout", follow_redirects=False)
    assert rv.status_code == 307

    c = urlparse(rv.location)
    dest = parse_qs(c.query)["post_logout_redirect_uri"][0]

    rv = oidc.get(dest, follow_redirects=True)
    assert rv.status_code == 200


def test_oidc_token(oidc):
    token = oidc.application.extensions["security"].datastore.token_cls(name="test", user_id=1)
    db = oidc.application.extensions["sqlalchemy"]
    with oidc.application.test_request_context():
        db.session.add(token)
        db.session.commit()
        token_id = token.id
        del token
        obj = db.session.get(oidc.application.extensions["security"].datastore.token_cls, token_id)
        assert obj.renewable
