# noinspection PyPackageRequirements
import pytest

from flask_cfmm.exceptions import log_failure_and_continue


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_log_and_continue(client, caplog):
    @log_failure_and_continue
    def suppress_exception():
        raise Exception

    caplog.clear()
    result = suppress_exception()
    assert result is False
    assert "suppress_exception failed:" in caplog.text


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_log_and_continue_success(client):
    @log_failure_and_continue
    def success():
        return True

    result = success()
    assert result is True


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_log_and_continue_fail(client, caplog):
    @log_failure_and_continue
    def fail():
        return False

    caplog.clear()
    result = fail()
    assert result is False
    assert "fail failed." in caplog.text


def test_token_invalid(oidc):
    rsp = oidc.get("/token", follow_redirects=False)
    assert rsp.status_code == 302, "TokenInvalid failed to logout"


def test_redirect(oidc):
    rsp = oidc.get("/redirect", follow_redirects=False)
    assert rsp.status_code == 302, "RedirectException should be redirected"
