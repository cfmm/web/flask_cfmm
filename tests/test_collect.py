from tempfile import TemporaryDirectory

from .conftest import create_app
from flask_cfmm.database import create_database


def test_collect(capsys):
    with TemporaryDirectory() as tmpdir:
        db = create_database()
        config = {
            "COLLECT_STATIC_ROOT": tmpdir,
            "KEYCLOAK_META_URL": "https://fake.example.com",
        }
        app = create_app(db, mapping=config)
        app.extensions["collect"].collect(verbose=False)
        captured = capsys.readouterr()
        assert captured.out.endswith("<<<CHANGED>>>\n")
        app.extensions["collect"].collect(verbose=False)
        captured = capsys.readouterr()
        assert not captured.out.endswith("<<<CHANGED>>>\n")


def test_collect_cli():
    with TemporaryDirectory() as tmpdir:
        config = {
            "COLLECT_STORAGE": "flask_collect.storage.file",
            "COLLECT_STATIC_ROOT": tmpdir,
            "KEYCLOAK_META_URL": "https://fake.example.com",
        }
        db = create_database()
        app = create_app(db, mapping=config)
        runner = app.test_cli_runner()
        result = runner.invoke(None, ["collect", "--idempotent", "--cleanup"])
        assert result.output.endswith("<<<CHANGED>>>\n")
        result = runner.invoke(None, ["collect", "--idempotent", "--no-cleanup"])
        assert not result.output.endswith("<<<CHANGED>>>\n")
