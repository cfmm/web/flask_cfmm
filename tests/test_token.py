from uuid import uuid4
import json


def test_token(oidc_app):
    auth = oidc_app.extensions["models"]["authorization"](token=str(uuid4()))

    content = {"test": "data"}

    data = auth.make_request(content)

    assert auth._validate_token(json.loads(data))
    assert auth.get_request() == content
