import glob
import os

import pytest
import flask_migrate
import sqlalchemy as sa
from alembic_utils import replaceable_entity

from flask_cfmm.migration import register_entities
from alembic_utils.replaceable_entity import registry


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_registration(client):
    try:
        register_entities(client.application.extensions["sqlalchemy"].engine)
        assert len(registry.entities()) == 2
    finally:
        replaceable_entity.registry.clear()


@pytest.mark.parametrize("client", ["postgresql", "postgresql_psycopg3", "mysql"], indirect=True)
def test_migrate_upgrade(client):
    os.chdir(os.path.dirname(__file__))
    files = glob.glob("migrations/versions/*")
    for file in files:
        try:
            os.remove(file)
        except OSError:
            pass
    db = client.application.extensions["sqlalchemy"]
    # noinspection PyUnresolvedReferences
    with client.application.app_context():
        db.session.remove()
        db.session.close()
        db.drop_all()

        # noinspection PyUnresolvedReferences
        try:
            version = db.session.execute(sa.text("SELECT * FROM alembic_version")).one_or_none()
            if version:
                flask_migrate.stamp(revision="base")
        except (sa.exc.OperationalError, sa.exc.ProgrammingError):
            pass

        # migrate/upgrade tables
        flask_migrate.migrate()
        flask_migrate.upgrade()

        # process triggers, views, functions and migrate/upgrade again
        try:
            register_entities(client.application.extensions["sqlalchemy"].engine)
            flask_migrate.migrate()
        finally:
            replaceable_entity.registry.clear()
        flask_migrate.upgrade()
        flask_migrate.upgrade()

        flask_migrate.downgrade(revision="base")
