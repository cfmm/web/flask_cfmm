from itertools import islice
from uuid import uuid1

# noinspection PyPackageRequirements
import pytest
from flask import current_app

from flask_cfmm.configure import get_role
from flask_cfmm.exceptions import BadRequest


# noinspection PyUnresolvedReferences
@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_authorization_deny(client):
    rv = client.get("/api/auth/none")
    assert rv.status_code == 403


# noinspection PyUnresolvedReferences
@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_unauthorized(client):
    with client.application.app_context():
        with current_app.test_request_context():
            current_app.login_manager.unauthorized()


# noinspection PyUnresolvedReferences
@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_authorization(client, users, auth_errors):
    username = next(islice(users, 1, None))
    _datastore = current_app.extensions["security"].datastore
    user = _datastore.create_user(username=username)
    _datastore.commit()
    role = _datastore.find_or_create_role(name=get_role("auth_role"))
    _datastore.add_role_to_user(user, role)
    key = uuid1()
    auth = current_app.extensions["models"]["authorization"](user=user, key=str(key), test=True)
    # noinspection PyUnresolvedReferences
    _datastore.db.session.add(auth)
    _datastore.commit()

    header = {"Authorization": auth.key}
    for flag in auth_errors:
        rv = client.get(f"/api/auth/{flag}", headers=header)
        assert rv.status_code == auth_errors[flag][1]


# noinspection PyUnresolvedReferences,PyUnusedLocal
def test_authorization_model(oidc):
    auth = current_app.extensions["models"]["authorization"]()
    db = current_app.extensions["sqlalchemy"]
    db.session.add(auth)
    db.session.commit()

    auth_id = auth.id
    del auth

    auth2 = db.session.get(current_app.extensions["models"]["authorization"], auth_id)
    with pytest.raises(BadRequest):
        auth2.get_request()


def test_authorization_cli(oidc):
    runner = oidc.application.test_cli_runner()
    uuid = str(uuid1())
    result = runner.invoke(None, ["authorization", "--upload", "--token", uuid, "--key", uuid])
    assert result.output.endswith("<<CHANGED>>\n")
    result = runner.invoke(None, ["authorization", "--upload", "--token", uuid, "--key", uuid])
    assert not result.output.endswith("<<CHANGED>>\n")
