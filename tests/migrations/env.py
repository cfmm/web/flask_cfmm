from alembic import context
from flask_cfmm.migration.utils import run_migrations_online, run_migrations_offline


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
