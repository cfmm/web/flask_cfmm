import uuid

# noinspection PyPackageRequirements
import pytest
from flask import current_app


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_create_user(client):
    username = "Test"
    _datastore = current_app.extensions["security"].datastore
    user1 = _datastore.create_user(username=username)
    _datastore.commit()
    user2 = _datastore.find_user(username=username)
    assert user1 == user2


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_create_user2(client, users):
    username = next(iter(users))
    _datastore = current_app.extensions["security"].datastore
    user2 = _datastore.find_user(username=username)
    assert user2.is_admin


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_find_user(client, users):
    username_a = next(iter(users))
    username_b = username_a.upper()
    assert username_a != username_b
    _datastore = client.application.extensions["security"].datastore
    user_a = _datastore.find_user(username=username_a)
    user_b = _datastore.find_user(username=username_b)
    assert user_b is None
    user_b = _datastore.find_user(username=username_a, case_insensitive=True)
    assert user_b == user_a


@pytest.mark.parametrize("client", ["oidc"], indirect=True)
def test_get_user_case(client, users):
    username = "Test_User"
    _datastore = client.application.extensions["security"].datastore
    user1 = _datastore.create_user(username=username.lower())
    _datastore.commit()
    userinfo = {
        "preferred_username": username,
        "email": f"{username}@example.com",
        "sub": uuid.uuid1(),
    }
    user2 = _datastore.get_user(userinfo)
    assert user1 == user2


def test_get_user(oidc):
    _datastore = current_app.extensions["security"].datastore
    assert _datastore.get_user(None) is None
