import os
from pathlib import Path

from flask import Flask, redirect, abort
from flask_admin.menu import MenuLink

from flask_security import current_user
from flask_admin import expose
from flask_cfmm import CFMMFlask, UserMixin, RoleMixin, Admin
from flask_cfmm.database import create_database
from flask_cfmm.model_views import UserView, ModelAdminView
from flask_cfmm.security import create_user


db = create_database()


class Role(RoleMixin, db.Model):
    pass


class User(UserMixin, db.Model):
    role_model = Role


class RouteFiltersView(ModelAdminView):
    column_filters = [
        "boolean_column",
    ]
    named_filter_urls = True


class InaccessibleView(UserView):
    @expose("/")
    def index_view(self):
        abort(403)


class AccessibleView(UserView):
    @expose("/")
    def index_view(self):
        return super().index_view()

    def is_accessible(self):
        return current_user.is_admin


def create_app():
    app = Flask(__name__)

    app.config.setdefault("KEYCLOAK_CLIENT_ID", os.getenv("CLIENT_ID"))
    app.config.setdefault("KEYCLOAK_CLIENT_SECRET", os.getenv("CLIENT_SECRET"))
    app.config.setdefault("KEYCLOAK_META_URL", os.getenv("CLIENT_URL"))
    app.config.setdefault("STATIC_URL_PATH", None)
    app.config.setdefault("CFMM_ADMIN_ROLE", "cfmm-u-admin")

    if True:
        db_path = Path(__file__).parent.joinpath("test.sqlite")
        db_path.unlink(missing_ok=True)
        app.config.setdefault("SQLALCHEMY_DATABASE_URI", f"sqlite:///{db_path}?check_same_thread=False")

    cfmm = CFMMFlask()
    cfmm.init_app(app, db, user_model=User)

    admin = Admin(app, base_template="cfmm/base.html")

    admin.add_link(MenuLink(name="Test", url="/admin/user", category="Links"))
    admin.add_view(UserView(User, db.session, endpoint="user"))
    admin.add_view(AccessibleView(User, db.session, endpoint="accessible", name="Accessible"))
    admin.add_view(InaccessibleView(User, db.session, endpoint="inaccessible", name="Inaccessible"))

    @app.route("/")
    def home():
        return redirect(admin.url + "/")

    return app


if __name__ == "__main__":
    application = create_app()
    application.app_context().push()
    db.create_all()

    # The first user gets the admin role, so create a fake admin user
    create_user("admin")

    application.run(port=5000)
